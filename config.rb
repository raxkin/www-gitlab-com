require 'generators/direction'
require 'generators/releases'
require 'generators/org_chart'
require 'extensions/breadcrumbs'
require 'lib/homepage'
require "thwait"

###
# Page options, layouts, aliases and proxies
###

# Disable HAML warnings
# https://github.com/middleman/middleman/issues/2087#issuecomment-307502952
Haml::TempleEngine.disable_option_validator!

# Per-page layout changes:
#
# With no layout
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

set :haml, {
  format: :xhtml
}

activate :syntax, line_numbers: false

set :markdown_engine, :kramdown
set :markdown, tables: true, hard_wrap: false, input: 'GFM'

activate :blog do |blog|
  blog.sources = 'posts/{year}-{month}-{day}-{title}.html'
  blog.permalink = '{year}/{month}/{day}/{title}/index.html'
  blog.layout = 'post'
  # Allow draft posts to appear on all branches except master (for Review Apps)
  blog.publish_future_dated = true if ENV['CI_BUILD_REF_NAME'].to_s != 'master'

  blog.summary_separator = /<!--\s*more\s*-->/

  blog.custom_collections = {
    categories: {
      link: '/blog/categories/{categories}/index.html',
      template: '/category.html'
    }
  }
  # blog.tag_template = '/templates/tag.html'
  # blog.taglink = '/blog/tags/{tag}/index.html'
end

activate :autoprefixer do |config|
  config.browsers = ['last 2 versions', 'Explorer >= 9']
end

activate :breadcrumbs, wrapper: :li, separator: '', hide_home: true, convert_last: false

# Reload the browser automatically whenever files change
unless ENV['ENABLE_LIVERELOAD'] != '1'
  configure :development do
    activate :livereload
  end
end

# Build-specific configuration
configure :build do
  set :build_dir, 'public'
  activate :minify_css
  activate :minify_javascript
  activate :minify_html

  # Populate the direction and releases pages only on master.
  # That will help shave off some time of the build times on branches.
  if ENV['CI_BUILD_REF_NAME'] == 'master' || !ENV.key('CI_BUILD_REF_NAME')
    # Direction page
    if ENV['PRIVATE_TOKEN']
      # Make variables available to threads by setting all to nil in this scope
      content = {}
      product_vision_content = direction_all_content = wishlist = nil
      # Create threads to fetch content for shared pages
      threads = []
      teams = %w[manage plan create verify package release configure monitor secure gitaly gitter geo]
      threads << Thread.new { wishlist = generate_wishlist } # wishlist, shared by most pages
      threads << Thread.new { product_vision_content = generate_product_vision } # /direction/product-vision/
      threads << Thread.new { direction_all_content = generate_direction(teams) } # /direction/
      teams.each do |name|
        # Create threads to fetch content for per-team pages
        threads << Thread.new { content[name] = generate_direction(%W[#{name}]) } # /direction/name/
      end
      ThreadsWait.all_waits(threads)
      # Set up proxies using now-fetched content for shared pages
      proxy '/direction/distribution/index.html', '/direction/distribution/template.html', locals: { wishlist: wishlist }, ignore: true
      proxy '/direction/product-vision/index.html', '/direction/product-vision/template.html', locals: { product_vision: product_vision_content }, ignore: true
      proxy '/direction/index.html', '/direction/template.html', locals: { direction: direction_all_content, wishlist: wishlist }, ignore: true
      teams.each do |name|
        # And for team pages
        proxy "/direction/#{name}/index.html", "/direction/#{name}/template.html", locals: { direction: content[name], wishlist: wishlist }, ignore: true
      end
    end

    ## Releases page
    releases = ReleaseList.new
    proxy '/releases/index.html', '/releases/template.html', locals: { list: releases.generate }, ignore: true
  end
end

org_chart = OrgChart.new
proxy '/company/team/org-chart/index.html', '/company/team/org-chart/template.html', locals: { team_data_tree: org_chart.team_data_tree }, ignore: true

# Proxy Comparison html and PDF pages
data.features.devops_tools.each_key do |devops_tool|
  next if devops_tool[0..6] == 'gitlab_'

  file_name = "#{devops_tool}-vs-gitlab".tr('_', '-')
  proxy "/devops-tools/#{file_name}.html", "/templates/comparison.html", locals: {
    key_one: devops_tool,
    key_two: 'gitlab_ultimate'
  }
  proxy "/devops-tools/pdfs/#{file_name}.html", '/devops-tools/pdfs/template.html', locals: {
    key_one: devops_tool,
    key_two: 'gitlab_ultimate'
  }, ignore: true
end

# Analyst reports
data.analyst_reports.each do |report|
  next unless report.url

  proxy "/analysts/#{report.url}/index.html", '/analysts/template.html', locals: {
    report: report
  }, ignore: true
end

# Category pages for /product
data.categories.each do |key, category|
  next if category.alt_link
  next unless category.available && (category.available <= Date.today)

  proxy "/product/#{key.dup.tr('_', '-').downcase}/index.html", '/product/template.html', locals: {
    category: category,
    category_key: key
  }, ignore: true
end

# Event pages
data.events.each do |event|
  next unless event.url

  proxy "/events/#{event.url.tr(' ', '-')}/index.html", '/events/template.html', locals: {
    event: event
  }, ignore: true
end

# Webcast pages
data.webcasts.each do |webcast|
  proxy "/webcast/#{webcast.url.tr(' ', '-')}/index.html", '/webcast/template.html', locals: {
    webcast: webcast
  }, ignore: true
end

# Reseller page
data.resellers.each do |reseller|
  proxy "/resellers/#{reseller.name.mb_chars.normalize(:kd).gsub(/[^\x00-\x7F]/n, '').downcase.to_s.tr(' ', '-')}/index.html", '/resellers/template.html', locals: {
    reseller: reseller
  }, ignore: true
end

# Release Radars /webcast/monthly-release
data.release_radars.each do |release_radar|
  proxy "/webcast/monthly-release/#{release_radar.name.tr(' ', '-').downcase}/index.html", '/webcast/monthly-release/template.html', locals: {
    release_radar: release_radar
  }, ignore: true
end

# GitLab Projects
proxy '/handbook/engineering/projects/index.html',
      '/handbook/engineering/projects/template.html',
      locals: { team: Gitlab::Homepage::Team.new },
      ignore: true

# Proxy case study pages
if @app.data.respond_to?(:case_studies)
  data.case_studies.each do |filename, study|
    proxy "/customers/#{filename}/index.html", '/templates/case_study.html', locals: { case_study: study }
  end
end

page '/404.html', directory_index: false

ignore '/templates/*'
ignore '/direction/template.html'
ignore '/direction/product-vision/template.html'
ignore '/direction/manage/template.html'
ignore '/direction/plan/template.html'
ignore '/direction/create/template.html'
ignore '/direction/verify/template.html'
ignore '/direction/package/template.html'
ignore '/direction/release/template.html'
ignore '/direction/configure/template.html'
ignore '/direction/monitor/template.html'
ignore '/direction/secure/template.html'
ignore '/direction/distribution/template.html'
ignore '/includes/*'
ignore '/releases/template.html'
ignore '/company/team/structure/org-chart/template.html'
ignore '/source/stylesheets/highlight.css'
ignore '/category.html'
ignore '/.gitattributes'
ignore '**/.gitkeep'

# See https://gitlab.com/gitlab-com/infrastructure/issues/4036
proxy '/development/index.html', '/sales/index.html'
