---
layout: markdown_page
title: "FY20-Q1 OKRs"
---

Note: This is the first quarter where we [shift our fiscal year](https://about.gitlab.com/handbook/communication/#writing-style-guidelines). FY20-Q1 will run from February 1, 2019 to April 30, 2019.

## On this page
{:.no_toc}

- TOC
{:toc}

### CEO: Grow Incremental ACV. More leads. Successful customers (standard implementation path, customer success based on their goals, DevOps maturity), Scalable marketing (Pipe-to-spend for all, double down on what works, more experiments with things like demo's, call us, and ebooks)

1. Alliances: Measure the value partners can bring to the business.  3 references for each major public/private cloud, establish customer ability to purchase through marketplaces, joint marketing with key partners - 200+ leads per event
1. CRO: Generate IACV from sales to higher tiers.  Target 250 new opportunities created from core accounts, Implement sales motion to show all starter renewal accounts the benefit and value of premium and ultimate, Identify two new already deployed large enterprise core customers in each SAL territory
1. CRO: Faster delivery of customer value.  Deliver plan to have Sales quote implementation services and success plan on all new account quotes, Launch standard implementation model in large segment, Measure time to completed implementation for all new enterprise accounts.
1. CMO: Recharge Inbound Marketing Engine. Top 10 Gitlab.com Pages optimized for inbound, Inbound trial and contact-us strategy documented and baselined, Pages optimized for top 20 non-branded SEO target terms, Inbound strategy for Dev, Sec & Ops personas launched
    1. Content Marketing: Broaden audience reach by increasing new user sessions on the blog by 15%. Create next iteration editorial mission statement and content framework. Publishing plan mapped to key topics, persona, and campaigns. Publish 5 interactive or deep-dive assets.
    1. Content Marketing: Increase social referral traffic 20%. Execute 2 social campaigns. Next iteration social strategy and framework. Create internal social advocacy program.
    1. Content Marketing: Build content paths to support lead flow. Create content strategies for 3 personas/buying groups. Execute plan to increase newsletter subscribers 50%. Contribute 10% of overall click-thrus to form fills.
    1. Digital Marketing programs: Improve Top 10 Gitlab.com Pages and increase inbound CTR and conversion rate by 10% QoQ.Optimize and increase visibility for 20 non-branded terms, Optimize competition pages for better visibility and to assist competitive take-out with 10% increase in CTR from Organic Google.
    1. Digital Marketing programs: 15% QoQ increase in inbound website leads through gated content and paid digital and increase conversion rate with improved nurture. Implement 3 baseline nurture tracks to educate and generate leads within the database, Implement an always-on digital paid program (to include re-targeting, etc.).
    1. Product Marketing: Document buyer’s journey (buyer’s experience, tribe needs). Documented handbook pages of messaging around the 3 core pillars of Dev, Sec & Ops.
    1. Product Marketing: Execute 1 competitive takeout campaign. Create campaign materials (web pages, demos, whitepapers, blogs), perform global sales enablement sessions.
    1. Product Marketing: Identify, evaluate and implement competitor analysis and comparisons strategy. Deliver first 5 comparisons using new strategy.
    1. Product Marketing: Improve case study publish rate.  Publish case studies at cadence of at least 2 per month, with at least 3 reference customers using all stages concurrently.
    1. Field Marketing: Create the ABM Framework and tooling. Execute ABM in 5 accounts per region, Identify messaging per account, Execute 1 account-targeted online campaign.
    1. Community Relations: Increase the number of GitLab CE and EE contributions (not contributors) from the wider community (including Core Team). For the 11.7 release, the target is 180; for the 11.8 release the target is 200; for the 11.9 release the target is 220.
    1. Community Relations: Accelerate open source project adoption of GitLab.com. Hire an Open Source Program Manager, gain commitment of 1 big open source project (1000+ contributors) to migrate to GitLab.
    1. Community Relations: Jumpstart meetups. Schedule 10 meetup events, at least one in every region (EMEA, APAC, NA).
1. CMO: Achieve marketing process and visibility excellence. 100% visibility into any and all leads at any time, Full-funnel marketing dashboard published in Looker, Unified marketing calendar published.
    1. Digital Marketing programs: Build out a framework and launch for an integrated campaign structure (ICS), execute one campaign, run one competitive take-out sub-campaign and adapt ICS to ABM use cases.
    1. Sales Development: BDR role clarity, defined development path. Clarified Lead Sources, add at least 2 Additional Lead Channels , inbound “call sales” phone line, Drift optimization - implement Drift Account Based Marketing.
    1. Sales Development: Reduce xDR Ramp Time to 3 months. Hire an Onboarding and Enablement Manager (Done), “Expert” Certifications around Personas, Product Tiers, better knowledge management solution: battlecards by persona, vertical.
    1. Field Marketing: Create Field Marketing Dashboard in SFDC. Identify & create the elements for dashboard, Creation of data driven decision framework for field, Improved visibility of field activity.
    1. Field Marketing: Create SLAs around Field Marketing & Marketing Ops. Creation of SLAs for Field Marketing to Ops (ops to field already exists), Create manual dashboard to track SLAs and improve performance, Improve overall speed of contact to MQL cycle time (need baseline).
    1. Community Relations: Increase responsiveness and coverage. Decrease median first reply time to 7 hours while adding two new response channels (GitLab forum, Stack Overflow).
1. CMO: Achieve $10.85m in IACV marketing sourced pipeline by beginning of Q2 FY19. 720 SAOs delivered to sales by SDRs/BDRs (SAOs as currently defined) @ $12,500 IACV avg, 14,285 MQLS created, hit 60% MQL accepted rate, hit 32% MQL qualified rate.
    1. Sales Development: Increase Accepted Opportunity per SDR to 8/month. Rise in reps achieving quota per month. 16 Jan, 18 Feb, 20 Mar, 22 Apr; > 1 significant (3 min+) conversation per rep per day; complete an Outreach.io audit and content improvement.

### CEO: Popular next generation product. Triple secure value (3 stages, multiple teams). Grown use of stages (SMAU). iPhone app on iPad.

1. Product: Increase product breadth. 25% (6 of 22) "new in 2019" categories at `minimal` maturity, CI/CD for iOS and Android development on GitLab.com, develop a Rails app on GitLab.com web IDE on iPad.
1. Product: Increase product depth for existing categories. Consumption pricing for Linux, Windows, macOS for GitLab.com and self-managed, 50% (11 of 22) "new in 2018" categories at `viable` [maturity](/handbook/product/categories/maturity/).
1. Product: Grow use of GitLab for all stages of the DevOps lifecycle. Increase Stage Monthly Active Users ([SMAU](/handbook/product/growth/#smau)) 10% m/m for each stage, 3 reference customers using all stages concurrently. => [SMAU Dashboard](https://gitlab.looker.com/dashboards/69)
1. Tech Writing: Sample 100 support tickets/interactions with customers created in Q1'19 and ensure 100% of all customer facing information used to resolve these issues is incorporated in our documentation in accordance with our [Docs-First Methodology](https://about.gitlab.com/handbook/documentation/#docs-first-methodology).
1. Alliances: evaluate qualifying for security competency with major clouds. Joint whitepaper, 1 security reference per major partner
1. Alliances: evaluate if key partner services (AKS,EKS,GKE, VKE, etc) can grow use of stages.  Conversion of SCM customers to CI/CD through joint GTM.
1. CRO:  Increase CI usage.  Perform CI workshop/whiteboard at 2 of the top 10 customers that are only using SCM today in each region/segment by end of Q1.
1. VPE
  1. Development: Increase velocity. Increase throughput 20% for all teams.
    1. Dev Backend: Communicate and address pain points with issue boards:
      identify and work with Product to prioritize 5 issues
    1. Dev Backend: Increase ownership for key GitLab components: add one
      new maintainer for each of Gitaly, Pages, Workhorse,
      and the Elasticsearch indexer
      1. Engineering Fellow: Reduce memory consumption for GitLab: address 2 main
        consumers in gitaly-ruby, reduce ProjectExportWorker usage by 50%
      1. Engineering Fellow: Make multithreaded server ready for production:
        instrument with Prometheus metrics, prepare production readiness review
      1. Geo: support Production team in preparing Geo for DR: enhance selective
        sync to work at scale, support GitLab Pages
      1. Manage: Take on more maintainer responsibilities: add a CE/EE backend maintainer
      1. Create: Improve maintainability and performance of GitLab Shell: port GitLab Shell to
        Golang
      1. Create: Take on more maintainer responsibilities: add a CE/EE backend
        maintainer, add a database maintainer
      1. Plan: Prepare Elasticsearch for use on GitLab.com. [Improve admin
        controls](https://gitlab.com/groups/gitlab-org/-/epics/428),
        [reduce index size](https://gitlab.com/groups/gitlab-org/-/epics/429)
    1. Ops Backend: Increase BE team's throughput by 20% and drive consistency week to week: Increase number of MRs week to week, increase number of reviewers and maintainers, and drive consistency in the trendline
       1. Verify: Increase Verify throughput by 20% and drive consistency week to week: Increase number of MRs week to week, increase number of reviewers and maintainers, and drive consistency in the trendline
       1. Release: Increase Release throughput by 20% and drive consistency week to week: Increase number of MRs week to week, increase number of reviewers and maintainers, and drive consistency in the trendline
       1. Configure: Increase Configure throughput by 20% and drive consistency week to week: Increase number of MRs week to week, increase number of reviewers and maintainers, and drive consistency in the trendline
       1. Monitor: Increase throughput and predictability, Increase throughput by 20% and even out throughput over the release cycles to bring the standard deviation below 2 for the most recent 12 weeks. As of Jan 4 the standard deviation is [2.8](https://www.wolframalpha.com/input/?i=standard+deviation+of+2,7,8,5,9,8,1,5,5,3,1,3)
       1. Secure: Increase Secure throughput by 20% and drive consistency week to week: Increase number of MRs week to week, increase number of reviewers and maintainers, and drive consistency in the trendline
      1. [Kamil](https://about.gitlab.com/company/team/#ayufanpl):
          1. Train 2 new maintainers of GitLab Rails,
          1. Help with building CD features needed to continuously deploy GitLab.com by closing 5 that are either ~"technical debt" or ~"bug"
      1. [Philippe Lafoucrière](https://about.gitlab.com/company/team/#plafoucriere): Handover to new Secure Engineering Manager.
      1. [Philippe Lafoucrière](https://about.gitlab.com/company/team/#plafoucriere): [CISSP](https://www.isc2.org/Certifications/CISSP) certification.
      1. [Philippe Lafoucrière](https://about.gitlab.com/company/team/#plafoucriere): Benchmark 3 new security tools for integration in the [Security Products](https://about.gitlab.com/handbook/engineering/ops-backend/secure/#security_products).
    1. Ops Backend: Define additional data to complement throughput: Implement a prototype for team pulse and define a path to implement it in GitLab product.
    1. Dev Frontend + Backend: GA of GraphQL to increase throughput for BE and FE. Resolve general topics like performance, abuse prevention and enjoyable development experience. Endpoints for Create and Plan relevant models. Implement new frontend features in Create and Plan only with GraphQL.
      1. Verify: Formulate approach to utilize GraphQL in areas where data is requested across multiple endpoints or custom datasets would increase performance.
      1. Release: Formulate approach to utilize GraphQL in areas where data is requested across multiple endpoints or custom datasets would increase performance.
    1. Frontend: Increase throughput and reduce implementation time of Frontend features by 20%. By having at least 50 gitlab-ui components at the end of Q1. Increase velocity of CSS adaptions. Improve tooling in GitLab itself for JS engineering (webpack map, visual diffing, etc.).
      1. Monitor: Create and start plan for removing css bloat and replacing GitLab components with components that match the design system.
      1. Verify: Reduce implementation time by building on top of gitlab-ui components; increase throughput by building/improving upon JS and Frontend specific tooling.
      1. Release: Reduce implementation time by building on top of gitlab-ui components; increase throughput by building/improving upon JS and Frontend specific tooling.
      1. Manage: Increase throughput via proper backlog grooming, improved estimation, and proper issue planning. Also utilize gitlab-ui components to reduce implementation time.
  1. Frontend: Improve Performance of JS computation that is done on every page by 10%. Improve Initialisation execution by 20%. Reduce main bundle size by 20%.
    1. Configure: ES2015+ bundles for modern browsers & transpiled ES5 for older ones should reduce bundle size and parsing time
    1. Manage: Fully integrate Sentry into the FE org workflow by updating the library, optimize and repair (loading order, configuration, sourcemap support), and determine process for fully utilizing it as an org. Also explore using it in dev, canary, and/or small percentage of production via A/B flipper.
  1. Infrastructure: Make all user-visible services ready for mission-critical workloads while seamlessly supporting delivery of new product functionality and integrating infrastructure solutions in GitLab. [Availability] Definition and tracking of GitLab.com metrics for SLAs, [Product] Deliver OKR Infrastructure workflow and tooling using GitLab, [Introspective] Achieve a 10% cost reduction on GitLab.com spending.
     1. Reliability[AS]: Support SRE-at-large MTBF and MTTR objectives. [Availability] Reduce spin-up time for provisioning auto-scaled servers to <3.5min, [Product] Automate error budgets as a GitLab feature, [Introspective] Deliver new testing environment.
     1. Reliability[DS]: Drive all user-visible services' MTBF to Infinity. [Availability] Deliver improvements to GitLab.com reliability on two foundational subsystems (ZFS on storage, Kubernetes + Charts), [Product] Take full operational ownership of CI/CD (take oncall from tomscz), [Introspective] Operationalize Vault and Consul.
     1. Reliability[JF]: Drive all user-visible services' MTTR to less than 60 seconds. [Availability] Implement 4 significant availability features to improve database reliability (autovacuum, masterless backups, pgrepack, replica on ZFS), [Product] Deliver Patroni on GitLab, [Introspective] Deliver full service operational inventory
     1. Delivery: Streamline GitLab deployments on GitLab.com and self-managed releases. [Availability] Create deployment rollback process, [Product] Reduce CE/EE pipeline runtime by 20%, [Introspective] Replace `takeoff` off with a method better suited for CI pipelines.
     1. Andrew Newdigate: Improve observability for all-user visible services to maximize MTBF and minimize MTTR. [Availability] Deliver distributed tracing on GitLab.com, [Product] Deliver Puma on GitLab and experiments on GitLab.com, [Introspective] Deliver overall service-level tracking in preparation for SLA tracking.
  1. Quality: Build everything in GitLab. Move GitLab Insights production ready graphs into GitLab itself.
  1. Quality: Improve GitLab performance. Continuous measure of GitLab memory usage based on real user scenarios via functional performance tests.
  1. Quality: Add Engineering Productivity metrics. Rollup dashboard for MR review time, MR size, rolling average time to resolution of P1 and P2 issues.
  1. Security: Secure the Product. Red Team with H1.
  1. Security: Secure the company. Evaluate at least 2 enterprise centralized SSO solutions and make selection. Evaluate at least 2 CBT-based secure coding training courses and work with People Ops to incorporate as part of developer onboarding process.
  1. Support: Update Handbook and Support Addendum to define Service Levels by plan and customer segment.
  1. Support: Author architecture document for 10k GitLab install with approval from Infrastructure.
  1. Support: Modify workflows so 50% of ticket resolutions include a link to existing documentation or an MR to enhance documentation.
  1. UX: Improve the onboarding experience for new GitLab users. Increase week 1 retention by X%, Create onboarding journeys for each of our personas.
  1. UX: Drive adoption of unknown/unused features. Create user journeys/flows for each stage group and determine X ways to introduce connections between them, Design a framework for onboarding users to unused features that increases adoption by 15% and can be reused across the application.
  1. UX: Make GitLab usable by everyone. Introduce accessibility testing to gitlab-ui/csslab, Fix X accessibility issues outlined in our VPAT.


### CEO: Great team. Employer brand (known for all remote, great communication of total compensation, 10 videos per manager and up), Effective hiring (Faster apply to hire), ELO score per interviewer), Decision making effectiveness (KPI's from original source and red/green, training for director group)

1. CFO: Improve financial reporting and accounting processes to support growth and increased transparency.
    1. Manager of Data Team: 100% of executive dashboards completed with goals and definitions
    1. Manager of Data Team: Public release of finance metric(s).
    1. Director of Business Operations: Data Integrity Process (DIP) completed for ARR, Net and Gross Retention and Customer counts.
    1. FinOps Lead: Integrated financial model that covers 100% of expense categories driven by IACV (as first iteration) and marketing funnel (as second iteration).
    1. FinOps Lead: Release and change control process for financial reporting documented and added to handbook.
    1. Sr Dir. of Legal: Detailed SoX compliance plan published internally and reviewed by Board.
    1. Sr Acctg Manager:  Key internal controls documented in handbook.
1. CFO: Create scalable infrastructure for achieving headcount growth
    1. Sr Dir. of Legal: 90% of team members covered by scalable employment solution
    1. Payroll and Payments Lead: Automation of contractor payroll completed.
1. CFO: Improve company wide operational processes
    1. Director of Bus Ops: Roadmaps for all staffed business operations functions are shipped and Q2 iteration by EOQ
    1. Controller: Zuora upgrade to orders which will allow for ramped deals, multiple amendments on single quote and MRR by subscription reporting.
1. VPE: Address compensation concerns. Develop a compensation roadmap document to effectively set expectations for every role in engineering, hold roundtable meeting for each role benchmark
    1. Ops Backend: Define first iteration for BE role framework (Software Engineer, Senior Software Engineer, Staff Software Engineer)
1. VPE: Invest in our existing team members. Unify vacancy descriptions and experience factor content, develop career matrices for all roles
  1. Development: Deliver the 2019 roadmap. Hire IC's to plan, Hire Directors for secure and enablement
  1. Development: Improve customer focus. Get every engineer in one customer conversation
    1. Dev Backend: Improve documentation and training for supporting our
      customers: identify and address 3 major pain points for customer support
      and/or professional services.
      1. Engineering Fellow: Help solve critical customer issues: Solve 1 P1
        issue
      1. Distribution: GA for GitLab Operator: GitLab Operator enabled by default
        in the charts
      1. Distribution: Create automated charts installation and upgrade pipelines for supported Kubernetes flavors (e.g. GKE, EKS, etc.)
      1. Gitter: Improve security practices: Enable use of dev.gitlab.org for
        security issues, document security release process for gitter.im in
        partnership with Security
      1. Gitaly: Make rapid progress on top company priorities: ship GA of object
        deduplication, ship beta of Gitaly HA
      1. Manage: Increase test coverage for the customers app: increase from 45%
        to 60%
      1. Manage: Proactively reduce future security flaws. Identify five areas
        where we have systematic/pervasive/repeated security flaws, Finalize
        plan to tackle three areas, 2 merge requests merged
      1. Create: Increase set of people working on Create features: have team
        deliver 6 Create Deep Dives
      1. Plan: Add to engineering blog to make it a more attractive
        destination for customers and prospective candidates. Four blog
        posts from the team on recent Plan work (including backstage)
      1. Distribution: Generate library licences and collection for the official Helm charts (similar to the omnibus-gitlab functionality)
  1. Ops Backend: Improve onboarding for new engineers: Improve team pages, resources, time to first MR merged by new engineers: Goal for first MR to be complete in the first 2 weeks.
      1. Verify: Improve onboarding for new engineers: Improve team page, resources, time to first MR merged by new engineers.
      1. Release: Improve onboarding for new engineers: Improve team page, resources, time to first MR merged by new engineers.
      1. Configure: Improve onboarding for new engineers: Improve team page, resources, time to first MR merged by new engineers.
      1. Monitor: Improve onboarding for new engineers: Improve team page, resources, time to first MR merged by new engineers.
      1. Monitor: Foster engineer growth. Each engineer has a career plan and measurable goals for the next 6 months: 0/5 engineers
      1. Secure: Improve onboarding for new engineers: Improve team page, resources, time to first MR merged by new engineers.
1. CCO: Improve the candidate experience at GitLab. This will require all members of the interview team to prioritize interviewing and inputting interview feedback quickly, in addition to process improvements and proper staffing of the recruiting team and wll be measured by a reduction to the time a candidate is in process to 30 days from application to Offer and 80% for all interviewers inputting their feedback within 24 hours.
1. CCO:  Improve GitLab's training and development progress, by hiring an L&D specialist (by the end of Q1), increasing the tuition reimbursement utilization by 10%, and defining metrics for each training that inform on the value of the training (80% of attendees rate the training as valuable and/or actionable, key business improvement metrics that should be impacted by the training, follow-up survey 30-60 after training to gauge applicability). The improvement should also be reflected in the annual Engagement survey.
1. CCO:  Improve GitLab's Employer brand, starting with hiring a Employer Branding specialist, identifying 5-10 locations to focus our branding and sourcing (25% increase in pipeline from these locations); collaborating with Marketing to ensure that recruiting activities are integrated into at least 95% for conferences, and increasing our responsiveness to Social Media posts (respond to 80% for Glassdoor posts)
