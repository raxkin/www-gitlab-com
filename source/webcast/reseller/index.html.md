---
layout: markdown_page
title: "Reseller Webcasts"
---

## On this page
{:.no_toc}

- TOC
{:toc}  


## Upcoming Webcasts  

### GitLab's security message

[Register](/webcast/reseller/security-message) to learn how to position GitLab’s security message and win business.

## OnDemand Webcasts 

### GitLab 101
[Register](/webcast/reseller/gitlab101-for-resellers/) to learn more about GitLab, the first single application to manage all stages of the DevOps lifecycle.

### Introduction to GitLab with GitBasics    
[Register](/webcast/reseller/git-basics) for a walkthrough of the GitLab user interface, its features and functionality and a demonstration of the GitLab flow for development. Also learn the basics of using Git and how to use it with GitLab. How to configure a local workspace, clone & pull from the GitLab, make local changes using the GitLab flow, commit them and finally push them back to the GitLab.    

### Introduction to GitLab's integrated CI/CD   
[Register](/webcast/reseller/cicd-ondemand) for an introductory course to using GitLab CI/CD, covering the .gitlab-ci.yml file focusing on some commonly used features and functionality and the GitLab Runner.   

### Product Tiers  
[Register](/webcast/reseller/product-tiers) to learn more about GitLab product tiers and maturity levels.

### Selling the value of GitLab Ultimate
[Register](/webcast/reseller/deep-dive/) to learn how you can help your enterprise customers solve their delivery challenges with GitLab Ultimate.

### Customer case studies
[Register](/webcast/reseller/customer-case-studies/) to hear how three customers leverage GitLab to gain a competitive advantage.

### GitLab's Security features - part 1
[Register](/webcast/reseller/security-features/) to learn about GitLab's Security features and why they are important to your customers

### GitLab's Security features - part 2
[Register](/webcast/reseller/security-features-part-2/) to learn about GitLab's Security features and why they are important to your customers