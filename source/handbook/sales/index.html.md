---
layout: markdown_page
title: "Sales Handbook"
---

## Reaching the Sales Team (internally)

- [**Public Issue Tracker**](https://gitlab.com/gitlab-com/sales/issues/) please use confidential issues for topics that should only be visible to team members at GitLab.
- Please use the 'Email, Slack, and GitLab Groups and Aliases' document for the appropriate alias.
- [**Chat channel**](https://gitlab.slack.com/archives/sales) please use the `#sales` chat channel for questions that don't seem appropriate to use the issue tracker or the internal email address for.

---
## On this page
{:.no_toc}

- TOC
{:toc}

---

## Other Sales Topics in Handbook

* [Sales Onboarding](/handbook/sales-onboarding/)
* [Business Operations](/handbook/business-ops)
* [GitLab Messaging and Positioning](/handbook/marketing/product-marketing/#elevator-pitch)
* [Sales Skills Best Practices](/handbook/sales-training/)
* [Sales Discovery Questions](/handbook/sales-qualification-questions/)
* [FAQ From Prospects](/handbook/sales-faq-from-prospects/)
* [How to Engage a Solutions Architect](/handbook/customer-success/solutions-architects#when-and-how-to-engage-a-solutions-architect)
* [How to Initiate a Refund](/handbook/finance/Accounts-receivable-and-cash/#sts=Refunds)
* [Client Use Cases](/handbook/use-cases/)
* [Proof of Concepts](/handbook/sales/POC/)
* [Account Planning Template for Large/Strategic Accounts](https://docs.google.com/presentation/d/1yQ6W7I30I4gW5Vi-TURIz8ZxnmL88uksCl0u9oyRrew/edit?ts=58b89146#slide=id.g1c9fcf1d5b_0_24)
* [Dealing with Security Questions From Prospects](/handbook/engineering/security/#security-questionnaires-for-customers)
* [Marketing & Sales Development](/handbook/marketing/marketing-sales-development/sdr/)
* [Who to go to to ask Questions or Give Feedback on a GitLab feature](/handbook/product/#who-to-talk-to-for-what)
* [How to conduct an executive meeting](https://www.youtube.com/watch?v=PSGMyoEFuMY&feature=youtu.be)
* [CEO Preferences when speaking with prospects and customers](/handbook/people-operations/ceo-preferences/#sales-meetings)


## Sales Resources outside of the Sales Handbook

* [Resellers Handbook](/handbook/resellers/)
* [Customer Reference Sheet](https://docs.google.com/a/gitlab.com/spreadsheets/d/1Off9pVkc2krT90TyOEevmr4ZtTEmutMj8dLgCnIbhRs/edit?usp=sharing)
* [Sales Collateral](https://drive.google.com/open?id=0B-ytP5bMib9TaUZQeDRzcE9idVk)
* [Sales Training](https://drive.google.com/open?id=0B41DBToSSIG_WU9LZ0o0ektEd0E)
* [Lead Gen Content Resources](/resources/)
* [GitLab ROI Calculator](/roi/?team_size=100&developer_cost=75)
* [GitLab University](https://docs.gitlab.com/ee/university/)
* [GitLab Support Handbook](/handbook/support/)
* [GitLab Hosted](/gitlab-hosted/)
* [Legal Page](/handbook/legal/)

# How GitLab Sales are organised

## Team Structure & Roles

See the GitLab [org chart](/company/team/org-chart).

Reporting into the Chief Revenue Officer (CRO) are the:

* VP Sales
* Director of Public Sector;
* Director of Channel;
* Director of Sales Operations;
* Director of Customer Success.

Reporting into the VP Sales are:
* Regional Sales Directors


#### Initial Account owner - based on [segment](/handbook/business-ops/#segmentation)

The people working with each segment and their quota are:

1. Strategic & Large: Strategic Account Leader (SAL) with a quota of $1M incremental annual contract value (IACV) per year
2. Mid market: Account Executive with a quota of $700,000 incremental ACV (IACV) per year
3. SMB: SMB Customer Advocate can close SMB deals directly by referring them to our self-serve web store or assisting them with a direct purchase.


The quota coverage types are:
1. Native Quota Req (NQR) - individual owns direct quota for their assigned territory.
2. Overlay Quota Rep (OQR) - individual supports an assigned territory that is currently owned by a NQR.


Quotas may be adjusted based on geographic region.


Variable Compensation takes the following into consideration:

* 70% of variable - Incremental ACV
* 5% of variable - Renewal ACV (Renewals with a negative iACV do not impact incremental acv quota number. Impact on negative IACV on renewals will only impact their renewal commission % on the amount renewed.)
* 25% of variable - Multi-year prepaid deals and professional services revenue.

#### Quota Ramp

For all quota carrying salespeople, we expect them to be producing at full capacity after 6 full months.  We have the following ramp up schedule and measure performance based on this schedule:

* Month 1 - 0%
* Month 2 - 0%
* Month 3 - 25%
* Month 4 - 50%
* Month 5 - 50%
* Month 6 - 75%
* Month 7+ - 100%

For the purposes of our internal analytics tool and forecasting we have the first 3 months at 25%.  We do this so we do not have overinflated %'s when a salesperson does sell something in months 1 and 2.

## Operating Rhythm - Sales Meetings, Presentations and Forecasting

### Bi-Weekly Monday Sales Call

* When: Every other Monday 9:00am to 9:50am Pacific time. The call is cancelled on observed US holidays that fall on Mondays.
* What: The agenda can be found by searching in Google Drive for "Sales Agenda". Remember that everyone is free to add topics to the agenda. Please start with your name and be sure to link to any relevant issue, merge request, or commit.

* General Guidelines

1. We start on time and do not wait for people.
1. If you are unable to attend, just add your name to the list of "Not Attending".
1. When someone passes the call to you, no need to ask "Can you hear me?" Just begin talking and if you can't be heard, someone will let you know.

* The call is recorded automatically, and all calls are transferred every hour to a Google Drive folder called "GitLab Videos". There is a subfolder called "Sales Team Meeting", which is accessible to all users with a GitLab.com e-mail account.
* The general order of the call is outlined below. Once the presenter has shared all of his/her items, he/she should then ask for questions. If there are no questions or all the questions have been answered, the presenter is to handoff to the next presenter on the agenda.
    1. The CRO or VP Sales will start the call with performance to-date for the month/quarter, then share the remaining forecast for the month/quarter. The CRO will then discuss any number of  topics ranging from pipeline cleanup, process reminders, and other  messages to the group. If the CRO is not attending, the Director of Sales Operations will share this update.
    1. The Director of Sales Operations will provide an update to process changes, new fields, and other updates related to sales process and group productivity.
    1. Marketing will provide an update to Sales. The Sales and Business Development Manager will provide an update on SDR and BDR performance to plan: pipeline created, meetings scheduled, and other SDR/BDR metrics. Other members of marketing may provide updates - new collateral, events, and other marketing-related activities.
    1. Customer Success is next. The Director of Customer Success may provide an update to an existing process or an introduction to a new process. In some cases, Solutions Architects may provide an update as well.
    1. Next, any topics from the group for discussion are encouraged. This can include questions on products, processes, or reminders to the rest of the group. In some cases, a summary of an event (client meetings, meetups, trade shows) will be shared.
    1. The call will conclude with a general 'scrum', which is designed for Account Executives and Account Managers to share anything new they have learned over the last week. This may include some competitive knowledge, new-found strategies or tactics that they have found helpful.
* If you cannot join the call, consider reviewing the recorded call or at minimum read through the sales team agenda and the links from there.

### Weekly Thursday Sales Enablement Call led by Product Marketing

* When: Happens every Thursday at 9am Pacific time

### Sales Group Conversation

* When: Happens once every 4 to 5 weeks at 8am Pacific time
* Where: Zoom link varies - please be sure to check the calendar invitation
* What: The CRO or VP Sales and Director of Sales Operations will provide the company with an update on sales team performance, notable wins, accomplishments, and strategies planned. You can view previous updates in the "Group Conversation Sales" (previously "Functional Group Update Sales") folder on the Google Drive.

### Weekly Forecasting

The Regional Director/Vice President of each team will establish weekly due date/time for your forecasts submissions. The RD/VP will be responsible for using your data for forecast the following:

* Gross IACV Commit & Best Case
* Renewal ACV Commit & Best Case

#### Forecast Category and Renewal Forecast Category Fields

* **Forecast Category** will be used when forecasting any opportunity with Incremental IACV. For example, if you are submitting a New Business, Add On Business, or Renewal opportunity to your forecast, the IACV portion of the opportunity will be included in your number.

* **Renewal Forecast Category** will be used when forecasting any renewal opportunities. In these cases, the Renewal ACV portion of the deal will be included into your forecast number.

#### Default Salesforce Stage to Forecast Category and Renewal Forecast Category Mapping

| **Opportunity Stage** | **Default Forecast Category** | **Renewal Forecast Category**
| -------- | -------- | -------- |
| 00-Pre Opportunity   | Omitted   |  NULL   |
| 0-Pending Acceptance   | Omitted   |  Commit   |
| 1-Discovery   | Pipeline   |  Commit   |
| 2-Scoping   | Pipeline   |  Commit   |
| 3-Technical Evaluation   | Pipeline   |  Commit   |
| 4-Proposal   | Best Case   |  Commit   |
| 5-Negotiating   | Commit   |  Commit   |
| 6-Awaiting Signature   | Commit   |  Commit   |
| Closed Won   | Closed   |  Closed   |
| 8-Closed Lost   | Omitted   | Omitted   |
| 9-Unqualified  | Omitted   | NULL   |
| 10-Duplicate   | Omitted   | NULL   |

#### Forecast Categories
There are two types of categories we review when your forecasts are pushed to Clari:

* **Closed**: Won Deals
* **Commit**: Won + Commit Deals
* **Best Case**: Won + Commit + Best Case
* **Pipeline**: Won + Commit + Best Case + Pipeline

#### Overriding Forecast Categories
Early in a quarter, you may not have a lot of opportunities in Stage 4 and beyond. With that in mind, you will be given the ability to override the Forecast Category and Renewal Forecast Category for your opportunities in both Salesforce and Clari.

In Salesforce, go to the Opportunities Home tab:
  1. Select either the *My Forecast* opportunity list view OR the *My Forecast* report.
  2. Click on the Opportunity Name of the record you want to update
  3. Change the **Forecast Category** and/or **Renewal Forecast Category** fields.
  4. Click Save.

In Clari, you will update records individually via Opportunity tab: 
  1. Select the *CQ: Open Deals*, *CQ: Commit Deals*, *CQ: Best Case*  or *CQ: Open Renewals* view
  2. You can update the opportunities directly within the opportunity grid. Just to to the field you want to update and double click.
  2. Click on the row of the opportunity you want to edit. On the right, you should see additional details (if you don’t, click on the *Toggle Details* button).
  2. In the Details tab, scroll to Forecast Category and double-click the field to edit. Select the desired category.
  3. Click Save.

##### Advantages to Updating Opps in Clari 
In Clari, we have provided a simplified layout meaning that we have designed the **Details** tab to include the most important fields that an RD/VP will review when determining whether they will include your opportunity in their regional forecast. **Purchasing Plan, MEDDPIC, Next Steps** are atop this simpliied layout. You won’t have to navigate through various sections of Salesforce to enter the most important details. What's more is that sales leadersihp will use this exact same view when reviewing your opportunities. So while you might be more familiar with updating opportunity records in Salesforce, over time, you should find updating in Clari will prove much more convenient.

#### Forecasting and Sales Leaders
If you are a Sales Leader, will have additional access to the **Forecasting Tab** in Clari. You will see four tabs:

* **Net IACV**
  * You will enter your Gross IACV Commit/Best Case and Renewal Loss Commit/Best Case in this tab. 
  * Gross IACV will be your bookings, a positive value; Renewal Loss will be for lost renewal ACV, represented by a negative value.
  * The result will be your Net IACV, which will be your official value submitted.

* **Gross IACV**
  * This will be a read only tab. You will see your rep’s quota, Won IACV, and pipeline coverage.

* **Renewal ACV**
  * This will be a read only tab. You will see your rep’s Closed Won Renewal ACV, Commit, Best Case, Renewal Loss Commit, and Renewal Loss Best Case.

* **TCV**
  * This will be a read only tab. You will see your rep’s Closed Won TCV, Commit, Best Case, and Pipeline.

##### Reviewing Forecasts

On the right of each tab, you will see a Toggle Details button. You can click on this to expand the list of opportunities in the selected forecast category.You will have three options:
* Total:  shows the combined deals for Closed + Open in that category (Closed, Commit, Best Case, or Pipeline)
* Closed: shows deals Closed Deals
* Open: shows open deals in that category

Click on the radio button for the type of opportunities you want to see (normally for forecasting calls with AEs, you will select “Open”). Then click “View Deals” to go to a full list of opportunities.

To overwrite your regional forecast:
1. In Clari, go to the Forecasting tab.
1. Go the tab of the metric you want to forecast (Gross IACV or Renewal ACV).
1. Go to Commit and override the value.
1. Add a note on why the adjustment made.
2. Repeat this process for Best Case.
1. Click Save.

#### Forecast Terminology
Please use these terms correctly and don't introduce other words. Apart from the above the company uses two other terms:

* **Plan**: Our yearly operating plan that is our promise to the board. The IACV number has a 50% of being too low and 50% chance of being too high but we should always hit the TCV - Opex number.
* **Forecast**: Our latest estimate that has a 50% of being too low and 50% chance of being too high.

## How Sales work with other GitLab teams

### Sales Development Representative (SDR)

#### 1:1 pairing with SDR

* Align on accounts and prioritize prospecting targets (both on the account and title levels).
* SDRs are to set 'at bat' meetings utilizing sales development best practices which are documented in the [SDR Handbook](/handbook/marketing/marketing-sales-development/sdr/#sdr-expectations)
* Drive brand awareness within target accounts

### Customer Success

See our [customer success page in the handbook for more details](/handbook/customer-success/).

### Support - Escalation to Support

During the sales cycle potential customers that have questions that are not within the scope of sales can have their queries escalated in different ways depending on the account size:

1. For Strategic/Large accounts that will have a dedicated Solutions Architect, [engage the SA](/handbook/customer-success/solutions-architects#when-and-how-to-engage-a-solutions-architect) so that they can triage and develop the request.
1. For questions that you think technical staff can answer in less than 10 minutes, see the [internal support](/handbook/support/#internal-support-for-gitlab-team-members) section of the support handbook.
1. If a potential customer has already asked you a question, forward a customer question via email to the **support-trials** email address. - It's important the email is **forwarded** and not CC'd to avoid additional changes required once the support request is lodged.

#### How Support Level in Salesforce is Established

Once a prospect becomes a customer, depending on the product purchased, they will be assigned a Support Level (Account) in Salesforce:

##### GitLab Self-managed
1. Premium: Ultimate customers; Premium customers; any Starter customer who has purchased the Premium Support add on
1. Basic: any Starter customer without the Premium Support add on
1. Custom: any customer on Standard or Plus legacy package

##### GitLab.com
1. Gold: Gitlab.com Gold Plan
1. Silver: Gitlab.com Silver Plan
1. Bronze: Gitlab.com Bronze Plan

If a customer does not renew their plan, their account will be transitioned to an "Expired" status.

## Product

### Who to talk to for what

If a client has a question or suggestion about a particular part of the product, find out [here](/handbook/product/#who-to-talk-to-for-what) who in the Product team you need to speak to.

### Contributing to Direction

Being in a customer facing role, salespeople are expected to contribute to [GitLab Direction](/direction/). Each day we talk to customers and prospects we have the opportunity to hear of a feature they find valuable or to give feedback (positive and constructive) to an feature that there is an issue for.
When you hear of feedback or you personally have feedback, we encourage you to comment within the issue, if one exists, or create your own issue on our [Issue Tracker](https://gitlab.com/gitlab-org/gitlab-ee/issues). Checking the [GitLab Direction](/direction/) page and the [Issue Tracker](https://gitlab.com/gitlab-org/gitlab-ee/issues) should happen throughout the week.

When you have an organization that is interested in a feature and you have commented in the issue and added a link to the account in salesforce, please follow the process outlined on the [product handbook](/handbook/product/#example-a-customer-has-a-feature-request) to arrange a call with the product manager and account to further discuss need of feature request.

## Requesting Assistance/Introductions Into Prospects from our Investors

We have great investors who can and want to help us penetrate accounts.  Each month we send out an investor update and within this update there will be a section called "Asks".  Within this section, we can ask investors for any introductions into strategic accounts. To make a request, within the account object in Salesforce, use the chatter function and type in "I'd like to ask our investors for help within this account".  Please cc the CRO and your manager within the chatter message.  All requests should be made before the 1st of the month so they are included in the upcoming investor update.

If an investor can help, they will contact the CRO, and the CRO will introduce the salesperson and the investor. The salesperson shall ask how the investor wants to be updated on progress and follow up accordingly.

### Email Intro

_This introduction email is progressive. You can choose to use just the first paragraph or the first two, three, or four depending on the detail you feel is appropriate for your audience._

GitLab is the end-to-end platform used by 10,000+ enterprises and millions of developers.  As the world is massively transforming the way software is developed and delivered, 10,000+ enterprises and millions of developers have done so using GitLab.

GitLab is the only single application built from the ground up for all stages of the DevOps lifecycle.  Companies including VMWare, Goldman Sachs, NVidia, Samsung, and Ticketmaster have all transformed and modernized their development process using GitLab.  In the process, they have made their software lifecycle 200% faster, reduced QA tasks from one hour to 30 seconds, all while also improving security and making developers massively more productive. 

With GitLab, Product, Development, QA, Security, and Operations teams are finally able to work concurrently on the same project. GitLab enables teams to collaborate and work from a single conversation, instead of managing multiple threads across disparate tools. GitLab provides teams a single data store, one user interface, and one permission model across the DevOps lifecycle allowing teams to collaborate, significantly reducing cycle time and focus exclusively on building great software quickly.

From project planning and source code management to CI/CD and monitoring, GitLab is a single application for [the entire DevOps lifecycle](https://about.gitlab.com/stages-devops-lifecycle/). Only GitLab enables [Concurrent DevOps](https://about.gitlab.com/concurrent-devops) to make the software lifecycle 200% faster.  Some additional detail is linked below:

* [Gitlab ranked number 4 software company (44th overall) on Inc. 5000 list of 2018's Fastest Growing Companies](https://about.gitlab.com/is-it-any-good/#gitlab-ranked-number-4-software-company-44th-overall-on-inc-5000-list-of-2018s-fastest-growing-companies)
* [GitLab has 2/3 market share in the self-managed Git market](https://about.gitlab.com/is-it-any-good/#gitlab-has-23-market-share-in-the-self-managed-git-market)
* [GitLab CI is the fastest growing CI/CD solution](https://about.gitlab.com/is-it-any-good/#gitlab-has-23-market-share-in-the-self-managed-git-market)
* [GitLab CI is a leader in the The Forrester Wave™](https://about.gitlab.com/is-it-any-good/#gitlab-ci-is-a-leader-in-the-the-forrester-wave)
* [GitLab is a top 3 innovator in IDC's list of Agile Code Development Technologies for 2018](https://about.gitlab.com/is-it-any-good/#gitlab-is-a-top-3-innovator-in-idcs-list-of-agile-code-development-technologies-for-2018)
* [GitLab is a strong performer in the new Forrester Value Stream Management Tools 2018 Wave Report](https://about.gitlab.com/is-it-any-good/#gitlab-is-a-strong-performer-in-the-new-forrester-value-stream-management-tools-2018-wave-report)
* [GitLab is one of the top 30 open source projects](https://about.gitlab.com/is-it-any-good/#gitlab-is-one-of-the-top-30-open-source-projects)
* [GitLab has more than 2,000 contributors](https://about.gitlab.com/is-it-any-good/#gitlab-has-more-than-2000-contributors)
* [GitLab has been voted as G2 Crowd Leader in 2018](https://about.gitlab.com/is-it-any-good/#gitlab-has-been-voted-as-g2-crowd-leader-in-2018)


## [GitLab Sales Process](/handbook/business-ops/#opportunity-stages)

### Capturing "MEDDPIC" Questions for Deeper Qualification

MEDDPIC is a proven sales methodology used for complex sales process into enterprise organizations. These questions should be answered in the "1-Discovery" Stage of the sales process. These questions will appear in the MEDDPIC Section of the Opportunity layout and will be required for all new business opportunities that are greater than $25,000 IACV.

* (M) Metrics: What is the economic impact of the solution?
  * Top line metrics include quicker time to market, higher quality- improvements towards sales and revenue.
  * Bottom line metrics include reduction in operating costs.
* (E) Economic Buyer: Who has profit/loss responsibility for this solution?
  * Economic Buyer (EB) has the power to spend.
  * Check with the EB on sponsorship, selection criteria.
* (D) Decision Criteria: Understand what is driving the decision?
  * What is the Technical Decision Criteria (TDC)?
  * What is the Business Decision Criteria (BDC)?
  * Check with both the Champion and the EB.
* (D) Decision Process: What will the decision-making process look like? Who is involved? What is their criteria for selection?
  *  What is the Technical Decision Process (TDM)?
  *  What is the Business Process (TBM)?
* (P) Purchasing Process: What does the purchasing process entail?
  * Is there a legal team involved in contract negotiations?
  * What does a purchase of this size look like?
  * Has a purchase of this size happened before?
* (I) Identify Pain: What are the primary objectives?
  * Is there a compelling event?
  * Remember, nobody buys until there is pain.
* (C) Champion: Who will sell on your behalf inside the company?
  * Is this person qualified to be your Champion?
  * Have you tested this person as your Champion?
  * Does this person know he/she is your Champion?

### Tracking Proof of Concepts (POCs)

In Stage 3-Technical Evaluation, a prospect or customer may engage in a proof of concept as part of the evaluation. If they decide to engage, please fill in the following information, as we will use this to track existing opportunities currently engaged in a POC, as well as win rates for customers who have engaged in a POC vs. those that do not.

1. In the opportunity, go to the 3-Technical Evaluation Section.
1. In the POC Status field, you can enter the status of the POC. The options are Not Started, Planning, In Progress, Completed, or Cancelled.
1. Enter the POC Start and End Dates.
1. Enter any POC Notes.
1. Enter the POC Success Criteria, ie how does the prospect/customer plan to determine whether the POC was successful or not.

### Capturing Purchasing Plans

Once you have reached Stage 4-Proposal, you should agree to a purchasing plan with your prospect/contact. This will include a clear understanding of purchase/contract review process and a close plan. This should include actions to be taken, named of people to complete actions and dates for each action. The information captured in this field will be used by sales management to see if the deal is progressing as planned. Delays in the purchasing plan may result in a request to push the opportunity into the following period.

## How Sales Segments Are Determined

Sales Segmentation information can be found in the [Business Operations - Database Management](/handbook/business-ops/#segmentation) section.

## Export Control Classification, and Countries We Do Not Do Business In

GitLab's Export Control Classification (or ECCN) is 5D992.c. As a consequence of this classification, we currently do not do business in: Iran, Sudan (excluding South Sudan), Syria, North Korea, and Cuba.

# Seeing how our customers use GitLab

## Adoption - GitLab Usage Statistics

Using [GitLab Version Check](/handbook/sales-process/version_check), GitLab usage data is pushed into Salesforce for both CE, EE and EE trial users. Once in Salesforce application, you will see a tab called "Usage Statistics". Using the drop down view, you can select CE, EE trails or EE to see all usage data sent to Gitlab.
Since version check is pulling the host name, the lead will be recorded as the host name.  Once you have identified the correct company name, please update the company name. Example: change gitlab.boeing.com to Boeing as the company name.

To view the usage, click the hyperlink under the column called "Usage Statistics".  The link will consist of several numbers and letters like, a3p61012001a002.
You can also access usage statistics on the account object level within Salesforce.  Within the account object, there is a section called "Usage Ping Data", which will contain the same hyperlink as well as a summary of version they are using, active users, historical users, license user count, license utilization % and date data was sent to us.

A few example of how to use Usage Statistics to pull insight and drive action?
* If prospecting into CE users, you can sort by active users to target large CE instances. You can see what they are using within GitLab, like issues, CI build, deploys, merge requests, boards, etc.  You can then target your communications to learn how they are using GitLab internally and educate them on what is possible with EE.
* For current EE users who are below their license utilization, you can engage the customer to understand their plan to rollout GitLab internally and how/where we can help them with adoption.
* For current EE users who are above their license utilization, you can leverage the data to engage the customer.  Celebrate the adoption of GitLab within their organization.  Ask why the adoption took off?  How they are using it (use cases)? Engage them in amending their contract right now for the add-on? Update the renewal opportunity to reflect the increase in usage for the true-up and new renewal amount.
* For current EE users who are not using a EE feature, i.e. CI or issues, you can engage the customer to understand why they are not using it.  Do they know we offer it? Are they using a competitive tool?  Have the integrated their current tool into GitLab? Are they open to learning more about what we offer to replace their current tool?
* For EE trials.  What EE features are they using and not using? If using a EE feature, what are they trying to solve and evaluate? If not using, why and are they open to evaluating that feature?

Take a look at a Training Video to explain in greater detail by searching for the "Sales Training" folder on the Google Drive.

## Open Source users - GitLab CE Instances and CE Active Users on SFDC Accounts

In an effort to make the [Usage/Version ping data](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html) simpler to use in SFDC, there are 2 fields directly on the account layout - "CE Instances" and "Active CE Users" under the "GitLab/Tech Stack Information" section on the Salesforce account record.

The source of these fields is to use the latest version ping record for and usage record for each host seen in the last 60 days and combine them into a single record. The hosts are separated into domain-based hosts and IP-based hosts. For IP based hosts, we run the IP through a [reverse-DNS lookup](https://en.wikipedia.org/wiki/Reverse_DNS_lookup) to attempt to translate to a domain-based host. If no reverse-DNS lookup (PTR record) is found, we run the IP through a [whois lookup](https://en.wikipedia.org/wiki/WHOIS) and try to extract a company name, for instance, if the IP is part of a company's delegated IP space. We discard all hosts that we are unable to identify because they have a reserved IP address (internal IP space) or are hosted in a cloud platform like GCP, Alibaba Cloud, or AWS as there is no way to identify those organizations through this data. For domain-based hosts, we use the [Clearbit](https://clearbit.com) domain lookup API to identify the company and firmographic data associated with the organization and finally the [Google Geocoding API](https://developers.google.com/maps/documentation/geocoding/start) to parse, clean, and standardize the address data as much as possible.

These stats will be aggregated up and only appear on the Ultimate Parent account in SFDC when there are children (since we don't really know which host goes to which child).

To see a list of accounts by # of CE users or instances, you can use the [CE Users list view](https://na34.salesforce.com/001?fcf=00B61000004XccM) in SFDC. To filter for just your accounts, hit the "edit" button and Filter for "My Accounts". Make sure to save it under a different name so you don't wipe out the original. These fields are also available in SFDC reporting.

A few caveats about this data:
* The hosts are mapped based on organization name, domain, and contact email domains to relate the instances to SFDC accounts as accurately as possible, but we may occasionally create false associations. Let the Data & Analytics team know when you find errors or anomalies.
* Both fields can be blank, but the organization can still be a significant CE user. Both fields being blank just means that there are no instances sending us version or usage pings, not necessarily that there are no active instances.
* Users can be the same person in multiple instances, so if a user exists in several instances at an organization with the usage ping on, they are counted each time they appear in an instance. Users may also be external to an organization, so the number of users may not represent employees and can thus be higher than the number of employees.
* These numbers represent the minimum amount that GitLab is likely being used within the organization, since some instances may have these pings turned on and some off. They also may have just the version ping on, in which case we won't see the number of users. This should not be interpreted as having instances but no users.

For the process of working these accounts, appending contacts from third-party tools, and reaching out, please refer to the [business operations](/handbook/business-ops/) section of the handbook.

# How we use our systems

## GitLab Tech Stack

For information regarding the tech stack at GitLab, please visit the [Business Operations section of the handbook](/handbook/business-ops#tech-stack) where we maintain a comprehensive table of the tools used across the Marketing, Sales and Customer Success functional groups, in addition to a 'cheat-sheet' for quick reference of who should have access and whom to contact with questions.

## Collaboration Applications used by Sales

Please [use the handbook](/handbook/handbook-usage/#why-handbook-first) as much as possible instead of traditional office applications.
When you have to use office applications always use [Gsuite applications](https://gsuite.google.com/together/) instead of Microsoft Office; for example, Google Sheets instead of Excel, Google Docs instead of Word, and Google Slides instead of Keynote or Powerpoint. Give the entire company editing rights whenever possible so everyone can contribute.

## Parent and Child Accounts

* A Parent Account is the business/organization which owns another business/organization.  Example: The Walt Disney Company is the parent account of Disney-ABC Television Group and Disney.com.
* A Child Account is the organization you may have an opportunity with but is owned by the Parent Account. A Child Account can be a business unit, subsidiary, or a satellite office of the Parent Account.
* You may have a opportunity with the Parent account and a Child Account.  Example: Disney and ESPN may both be customers and have opportunities. However, the very first deal with a Parent Account, whether it is with the Parent Account or Child Account, should be marked as "New Business". All other deals under the Parent Account will fall under Add-On Business, Existing Account - Cross-Sell, or Renewal Business (see Opportunity Types section).
* If the Parent and Child accounts have the same company name, either add the division, department, business unit, or location to the end of the account name. For example, Disney would be the name of the Parent Account, but the Child Account would be called The Walt Disney Company Latin America or The Walt Disney Company, Ltd Japan.
* When selling into a new division (which has their own budget, different mailing address, and decision process) create a new account.  This is the Child Account.  For every child account, you must select the parent account by using the parent account field on the account page. If done properly, the Parent/Child relationship will be displayed in the Account Hierarchy section of the account page.
* Remember that a child account can also be a parent account for another account. For example, Disney-ABC Television Group is the child for The Walt Disney Company, but is the parent for ABC Entertainment Group.
* We want to do this as we can keep each opportunity with each child account separate and easily find all accounts and opportunities tied to a parent account, as well as roll-up all Closed Won business against a Parent Account.

## When to Create an Opportunity with Software License Fees

Before a LEAD is converted or an OPPORTUNITY is created the following must occur:

1. Authority
  * What role does this person play in the decision process? (i.e. decision maker, influencer, etc.) OR is this someone that has a pain and a clear path to the decision maker
1. Need
  * Identified problem GitLab can solve
1. Required Information
  * Number of potential paid users
  * Current technologies and when the contract is up
  * Current business initiatives
1. Handoff
  * The prospect is willing to schedule another meeting with the salesperson to uncover more information and uncover technical requirements and decision criteria
1. Interest by GitLab salesperson to pursue the opportunity.  Are you interested in investing time in pursuing this opportunity.

* When creating any OPPORTUNITY be sure to review the criteria needed for each [Opportunity Stage](/handbook/business-ops/#opportunity-stages) and be aware that all opportunities must enter the "O-Pending Acceptance" stage or you will encounter a validation rule error.

### Creating an Opportunity

Opportunities can only be created *from* a CONTACT record or when *converting* a LEAD. See the ['How to create an Opportunity'](/handbook/business-ops/#how-to-create-an-opportunity) section of the Business OPS handbook.


## True up

***What is it?***

A true up is a back payment for a customer going over their license count during the year.

***Why do we use it?***

We use the true up model so that the license never becomes a barrier to adoption for the client.

***Let's take an example.***

A customer takes out a subscription for 100 users.  During the year they grow the usage of the server and 200 additional users come on board.  When it comes time to renew, they are now up to 300 active users despite the license only being for 100 users.

At this point the customer needs to do two things: they need to renew the license and to add the true up users.  The number of true up users in this case is 200.  It is the difference between the `Maximum Users` and the `Users in License`.  This can be found by the customer in their GitLab server by going to the Admin > License area.  It will look like [this](https://docs.gitlab.com/ee/user/admin_area/img/license_details.png)

There is more information below on the steps you need to take, but in this example you would need to update the number of users for the renewal to 300 and add the `True Up` product to the renewal quote for 200 users.  This will create a one time only charge to the customer and in a year's time, they will have a renewal for 300 users.

[***See here more information on the steps to take for the true up***](/handbook/sales/#renew-existing-subscription-with-a-true-up-for-the-billing-account)

***Note that we make provision for true ups in our standard [Subscription Terms](/terms/#subscription) Section 5 - Payment of Fees.***

## Deal Sizes

Deal Size is a dimension by which we will measure stage to stage conversions and stage cycle times of opportunities. Values are [IACV](/handbook/finance/operating-metrics/#bookings-incremental-annual-contract-value-iacv) in USD.

1. Jumbo - USD 100,000 and up
1. Big - USD 25,000 to 99,999.99
1. Medium - USD 5,000 to 24,999.99
1. Small - below USD 5,000

## Associating Emails to Opportunities

Associating emails to Salesforce is a best practice that should be followed by all users. Attaching emails to a Lead, Account, Contact, or Opportunity record are vital for transparency, as any number of team members can review the correspondence between GitLab and the customer/prospect account. It doesn't do anyone any good when all of the correspondence is kept in a user's inbox.

With that said, there are multiple ways that an email can be associated to an opportunity. But before you do that, you must first make sure that the contact record is associated to an opportunity via the Contact Roles object.

### First, Contact Roles

To add a Contact Role to an opportunity:
1. Go to the Opportunity.
1. Go to the Contact Role related list.
1. Click 'New'.
1. Add the Contact.
1. If this person is your primary contact, click on the 'Primary' radio button.
1. Add the Role this person plays in the opportunity (Economic Buyer, Technical Buyer, etc).
1. Repeat the steps, although you can only have one Primary Contact per opportunity.
1. For more information, you can visit the [Salesforce Knowledge Center](https://help.salesforce.com/articleView?id=contactroles_add_cex.htm&type=5).

### Email to Salesforce

Email to Salesforce allows you to associate emails to Opportunities using an email long email string that associates the email to the contact. To set this up:

1. Click on your 'Name' on the top right of any page in Salesforce.
1. Select 'My Settings'
1. Go to the left sidebar and click 'Email'.
1. Then click 'My Email to Salesforce'.
1. Copy the email string.
1. Go to My Acceptable Email Addresses and make sure your gitlab email address is there.
1. In 'Email Associations', make sure "Automatically assign them to Salesforce records" is checked.
1. Check Opportunities, Leads, and Contacts.
1. In the Leads and Contacts section, make sure to select "All Records".
1. It is up to you if you want to save all attachments, as well as if you want to receive an email confirmation of an association (my recommendation is yes for the first few weeks to make sure it's working, then you can turn it off anytime).
1. Click Save.
1. Go to Gmail and save this email address in a Contact record. A good practice is name it 'BCC SFDC' for first and last name.
1. When you send any emails from Gmail, you will BCC the "BCC SFDC" contact, which will send the email to long string you copied in Step 5.

### Outreach

If you want to associate emails to Opportunities using Outreach, follow these steps:

1. Go to your photo on the bottom left.
1. Click Settings
1. Select 'Plugins' on the left menu.
1. Select the SFDC (Salesforce) plugin.
1. Click on 'Contact'.
1. On the bottom right, enable 'Automatically associate activity with Opportunity'
1. Click the 'Save' button on the top right.

### Salesforce Lightning for Gmail

If you want to associate emails to Opportunities or other records using the Salesforce Lightning for Gmail plug in, follow these steps:

1. Visit the Chrome Store to download the [Salesforce Lightning for Gmail](https://chrome.google.com/webstore/detail/salesforce-lightning-for/jjghhkepijgakdammjldcbnjehfkfmha) plug in.
1. Click `Add to Chrome`
2. Click `Add Extension`
2. Go to Gmail and open the right sidebar.
3. When you open an email that may contain email addresses from existing leads or contacts in Salesforce, all related records associated to that email (Lead, Contact, Account, Opportunity, and Cases) will appear, and you can select any or all records to related the email to.
3. For each record you'd like to associate the email to, click on the upload icon for each record.
4. If you wish, you can include some or all of the attachments to the record as well by clicking the checkbox next to each attachment.

If you have any issues with the setup, please contact the Director of Sales Operations via Slack, email, or SFDC Chatter.

## Sales Order Processing

See the [order processing](/handbook/business-ops/order-processing/) page for information and training related to using Salesforce to create accounts, contacts, opportunities, etc.; opportunity reassignments; returning customer creation; and more.
