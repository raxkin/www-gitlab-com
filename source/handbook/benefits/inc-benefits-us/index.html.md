---
layout: markdown_page
title: "GitLab Inc (US) Benefits"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Specific to US based employees
{: #us-specific-benefits}

US based employees' benefits are arranged through [Lumity](https://lumity.com/). The benefits decision discussions are held by People Operations, the CFO, and the CEO to elect the next year's benefits by the carrier's deadlines. People Operations will notify the team of open enrollment as soon as details become available.    

If you have any questions regarding benefits please reach out to People Operations directly. If you have any questions with the Lumity platform, feel free to reach out to their support personnel at `support@lumity.com`.

Carrier ID cards are normally received within weeks of submitting your benefit elections. If you or your medical providers are in need of immediate confirmation of your coverage, please contact the carrier directly.

GitLab covers **100% of employee contributions and 66% for employee's spouse, dependents, and/or domestic partner** of premiums for medical, dental, and vision coverage. Plan rates are locked through December 31, 2019.

More information on the processed deductions in payroll from Lumity can be found in the [Finance handbook](https://about.gitlab.com/handbook/finance/processing-payroll/#lumity-payroll-processes-for-gitlab-inc).

## Open Enrollment

GitLab has opened a passive open enrollment for the plan year 2019-01-01 to 2019-12-31 via Lumity. If you do not want to change any Medical, Dental, or Group Benefits no action needs to be taken; your rates and coverage will stay the same through 2019. If you would like an FSA for the 2019 calendar year you **must** actively elect this during the December open enrollment period. The deadline for the passive Open Enrollment is 2018-12-15. 

GitLab held a 101 with Lumity around open enrollment:
  * Recording from the call with Lumity https://vimeo.com/284765746/46c006d89c
  * Slide deck presented in the call:  https://drive.google.com/file/d/0B4eFM43gu7VPa2YzZkpzNmxMQXJjS3JCZm5reE5UZWtoMlR3/view?usp=sharing.

## Group Medical Coverage

GitLab offers plans from United Health Care for all states within the US as well as additional Kaiser options for residents of California, Hawaii, and Colorado. Deductibles will run 2018-10-01 to 2018-12-31 and reset 2019-01-01 for a plan year of 2019-01-01 to 2019-12-31.  

For additional information on how [Benefits](https://drive.google.com/file/d/0B4eFM43gu7VPVS1aOFdRdXRSVzZ5Z3B0dEVKUXJIWm1zZXRj/view?usp=sharing) or [HSAs](https://drive.google.com/file/d/0B4eFM43gu7VPbUo1VFNFVFlQNlUyV0xQZkE0YXQyZENSNU1j/view?usp=sharing) operate, please check out the documentation on the Google Drive or Lumity's [resources](https://employee-resources.lumity.com/help).

_If you already have current group medical coverage, you may choose to waive or opt out of group health benefits. If you choose to waive health coverage, you will receive a $300.00 monthly benefit allowance and will still be able to enroll in dental, vision, optional plans, and flexible spending accounts._

If you do not enroll in a plan within your benefits election period, you will automatically receive the medical waiver allowance.

GitLab has confirmed that our medical plans are CREDITABLE. Please see the attached [notice](https://docs.google.com/document/d/1kZ3P51ZmCL5EnFb4Tv8nEB9Qgb8tlbwASV9qjiSWqEs/edit?usp=sharing). If you or your dependents are Medicare eligible or are approaching Medicare eligibility, you will need this notice to confirm your status when enrolling for Medicare Part D.

### Eligibility

Any active, regular, full-time employee working a minimum of 30 hours per week are eligible for all benefits. Benefits are effective on your date of hire. Others eligible for benefits include:
  * Your legal spouse or domestic partner,
  * Your dependent children up until age 26 (including legally adopted and stepchildren), and/or
  * Any dependent child who reaches the limiting age and is incapable of self-support because of a mental or physical disability

### United Health Care Medical Plans

**Coverages:**

| Plan Details               | UHC - HSA              | UHC - EPO       | UHC - PPO         |
|:---------------------------|:----------------------:|:---------------:|:-----------------:|
| Deductible (Single/Family) | $2,000 / $2,700        |   $0 / $0       |  $500 / $1,000    |
| OOP Max (Single/Family)    | $4,000 / $4,000        | $2,500 / $5,000 | $3,000 / $6,000   |
| PCP/Specialist Copay       | 20% / 20%              | $20 / $20       | $20 / $20         |
| Coinsurance                | 20%                    | 0%              | 10%               |
| Emergency Room             | 20%                    | $100            | $100              |
| Urgent Care                | 20%                    | $50             | $50               |
| Hospital Inpatient         | 20%                    | $250            | 10%               |
| Hospital Outpatient        | 20%                    | 0%              | 10%               |
| Rx - Deductible            | -                      | -               | -                 |
| Generic                    | $10                    | $10             | $10               |
| Brand - Preferred          | $30                    | $30             | $30               |
| Brand - Non-Preferred      | $50                    | $50             | $50               |
| Specialty Drugs            | $50                    | 20% up to $250  | 20% up to $250    |
| Plan Specifics             | AXEX (Select Plus HSA) | PVK (Select)    | PR1 (Select Plus) |
| Rx Plan                    | C2-HSA                 | Rx Plan: 464    | Rx Plan: 464      |

**Employee Costs:**

The following costs are monthly rates which would be deducted from your paycheck.

| Tier                  | HSA | EPO  | PPO  |
|-----------------------|:---:|:----:|:----:|
| Employee Only         | $0  | $0   | $0   |
| Employee + Spouse     | $0  | $168 | $168 |
| Employee + Child(ren) | $0  | $144 | $144 |              
| Family                | $0  | $312 | $312 |

Note: For the employee only HSA, GitLab will contribute $100 per month. For residents of California, Alabama, and New Jersey this additional contribution is taxable on the state level. If you are 55 or order and would like to contribute an additional amount for the HSA catch up, please reach out to support@lumity.com and People Ops at GitLab.

Domestic Partner Reimbursements: If the employee is not legally married to his/her domestic partner, the domestic partner’s expenses are not eligible for disbursement from the HSA. However, if the domestic partner is covered under the family HDHP through the employee, the domestic partner can open his/her own HSA and contribute up to the full family contribution maximum. The employee may also contribute up to the full family contribution maximum to their own HSA.

### Kaiser Medical Plans

**Coverages:**

| Plan Details               | HMO 20 NorCal        | HMO 20 SoCal    | HMO 20 CO       | HMO 20 HI       |
|----------------------------|:--------------------:|:---------------:|:---------------:|:---------------:|
| Deductible (Single/Family) | $0                   | $0              | $0              | $0              |
| OOP Max (Single/Family)    | $1,500 / $3,000      | $1,500 / $3,000 | $2,000 / $4,000 | $2,500 / $7,500 |
| PCP/Specialist Copay       | $20 / $35            | $20 / $35       | $20 / $35       | $20 / $20       |
| Coinsurance                |                      |                 |                 | 10%             |
| Emergency Room             | $100                 | $100            | $100            | $100            |
| Urgent Care                |                      |                 |                 |                 |
| Hospital Inpatient         | $250/admit           | $250/visit      | $300/admit      | 10%             |
| Hospital Outpatient        | $35/procedure        | $35/procedure   | $100            | 10%             |
| **Rx - Deductible**        |                      |                 |                 |                 |
| Generic                    | $10                  | $10             | $10             | $3 / $15        |
| Brand - Preferred          | $35                  | $35             | $30             | $50             |
| Brand - Non-Preferred      | $35                  | $35             | $50             | $50             |

**Employee Costs:**

The following costs are monthly rates which would be deducted from your paycheck.

| Tier                  | HMO CA North | HMO CA South | HMO CO | HMO HI |
|-----------------------|:------------:|:------------:|:------:|:------:|
| Employee Only         | $0           | $0           | $0     | $0     |
| Employee + Spouse     | $336         | $216         | $280   | $240   |
| Employee + Child(ren) | $280         | $168         | $240   | $480   |          
| Family                | $560         | $336         | $456   | $480   |

### Infertility Services

Infertility services are provided through the UHC HSA plan. Please consult the legal SBC for more information or reach out to People Operations.

### Transgender Medical Services

Recently, the [United States Department of Health and Human Services](https://www.hhs.gov/) released a [mandate on transgender non-discrimination](http://www.transgendermandate.org/). As part of this mandate, medical carriers were given time to review their current policies and update to reflect the mandate. Because there is a wide range of services, treatment, and goals for transgender patients, employees are encouraged to contact their carrier directly to have these discussions.

### Pregnancy & Maternity Care

With medical plans, GitLab offers pregnancy and maternity care. Depending on the plan you selected, your coverages may differ for in-network vs out-of-network, visits, and inpatient care. Please contact Lumity or People Operations with any questions about your plan.

## Dental

Dental is provided by Cigna, plan: DPPO.

Dental does not come with individualized insurance cards from Cigna, although you can download them by setting up a Cigna account through https://my.cigna.com. Lumity's site will house individualized ID cards employees can access at any time. For the most part, dental providers do not request or require ID cards as they look up insurance through your social security number. If you need additional information for a claim please let People Ops know. Cigna'a mailing address is PO Box 188037 Chattanooga, TN, 37422 and the direct phone number is 800-244-6224.

**Coverages:**

| Plan Details                         | DPPO       |  
|--------------------------------------|:----------:|
| Deductible                           | $50 / $150 |
| Maximum Benefit                      | $2,000     |
| Preventive Care CoInsurance (in/out) | 0% / 0%    |
| Basic Care Coinsurance (in/out)      | 20% / 20%  |
| Major Care Coinsurance (in/out)      | 50% / 50%  |
| Out of Network Reimbursement         | 90th R&C   |
| **Orthodontia**                      |            |
| Orthodontic Coinsurance (in/out)     | 50% / 50%  |
| Orthodontic Max Benefits             | $1,500     |

**Employee Costs:**

The following costs are monthly rates which would be deducted from your paycheck.

| Tier                  | DPPO |
|-----------------------|:----:|
| Employee Only         | $0   |
| Employee + Spouse     | $12  |
| Employee + Child(ren) | $18  |           
| Family                | $36  |

## Vision

Vision is provided by Cigna.

**Coverages:**

| Plan Details                      | Vision       |  
|-----------------------------------|:------------:|
| Frequency of Services             | 12 / 12 / 12 |
| Copay Exam                        | $20          |  
| Copay Materials                   | -            |
| Single Vision                     | $0           |  
| Bifocal                           | $0           |
| Trifocal                          | $0           |
| Frame Allowance                   | up to $130   |
| Elective Lenses Contact Allowance | up to $130   |

**Employee Costs:**

The following costs are monthly rates which would be deducted from your paycheck.

| Tier                  | Vision |
|-----------------------|:------:|
| Employee Only         | $0     |
| Employee + Spouse     | $2.40  |
| Employee + Child(ren) | $1.80  |           
| Family                | $4.80  |

## Discovery Benefits

If you are enrolled in an HSA, FSA, or commuter benefits, the funds are held through Discovery Benefits. FSAs have a $500 rollover each calendar year.

You will only receive on debit card upon enrollment. To obtain a second card (for a dependent, etc.) you will need to login to your account on Discovery or call and they will send one to your home of record.

If you would like to transfer your HSA from a previous account, please contact Discovery Benefits and request a HSA Transfer funds form. On the form you will put your old HSA provider’s account number and any other required personal information. You will then submit the form to Discovery, and they will get in contact with your old HSA provider and process the transfer of funds. You can reach Discovery Benefits at 866.451.3399.

## Basic Life Insurance and AD&D

GitLab offers company paid basic life and accidental death and dismemberment (AD&D) plans through Cigna. The Company pays for basic life insurance coverage valued at two times annual base salary with a maximum benefit of $250,000, which includes an equal amount of AD&D coverage.

## Group Long-Term and Short-Term Disability Insurance

GitLab provides a policy through Cigna that may replace up to 66.7% of your base salary, for qualifying disabilities. For short-term disability there is a weekly maximum benefit of $2,500; for long-term disability there is a monthly benefit maximum of $12,500.

For the process on how to apply for STD please review [Applying for Parental Leave](/handbook/benefits/inc-benefits-us/#apply-for-parental-leave-in-the-us). The process for the application is the same, but the reason will be unique to the disability instead of a birth related disability.

## 401k Plan

The company offers a 401k plan in which you may make voluntary pre-tax contributions toward your retirement. We do not currently offer matching contributions.

### Administrative Details of 401k Plan

1. You are eligible to participate in GitLab’s 401k as of your hire date. There is no auto-enrollment. You must actively elect your deductions.
1. You will receive an invitation from [Betterment](www.betterment.com) who is GitLab's plan fiduciary. For more information about Betterment please checkout this [YouTube Video](https://www.youtube.com/watch?v=A-9II-zBq1k).
1. Any changes to your plan information will be effective the first of the following month.
1. Once inside the platform you may elect your annual/pay-period contributions and investments.
1. Please review the [Summary Plan Document](https://drive.google.com/file/d/1k1xWqZ-2HjOWLv_oFCcyBkTjGnuv1l_B/view?usp=sharing) and [QDIA & Fee Disclosure](https://drive.google.com/file/d/1gWxCI4XI01gofUu9yqKd4QyXeM27Xv57/view?usp=sharing). If you have any questions about the plan or the documents, please reach out to People Ops. 

Assets will be transferring from TransAmerica (via TriNet) to Betterment on November 12, 2018. No action is necessary for this transfer. There will be a [blackout period](https://drive.google.com/file/d/1tDgUjasApa0jt1cyJvrZtCVe73RZL5pZ/view?usp=sharing) while the assets are being transferred.

**Timeline for the Blackout Period:**
2018-11-02	Last day to submit all transactions (by 3PM CT)      
2018-11-05	Blackout start date
2018-11-09	Liquidation date                                                                                    
2018-11-12	Wire Transfer date

## Optional Plans Available at Employee Expense

### Flexible Spending Account (FSA) Plans

FSAs help you pay for eligible out-of-pocket health care and dependent day care expenses on a pretax basis. You determine your projected expenses for the Plan Year and then elect to set aside a portion of each paycheck into your FSA.

### Supplemental Life/Accidental Death and Dismemberment Insurance

If you want extra protection for yourself and your eligible dependents, you have the option to elect supplemental life insurance. You may elect:
   * $10,000 Increments up to the lesser of 6x annual salary or $750,000 for employees
   * $5,000 Increments up to the lesser of $250,000 or 100% of employee election for spouses and domestic partners
   * $10,000 of coverage available for children

Certain coverage increases must be approved by the insurance carrier.

### Commuter Benefits

GitLab offers [commuter benefits](https://drive.google.com/file/d/0B4eFM43gu7VPek1Ia0ZqYjhuT25zYjdYTUpiS1NFSXFXc0Vn/view?usp=sharing) which are administered through Discovery Benefits. The contribution limits from the IRS for 2019 are $265/month for parking and $265/month for transit. These contributions rollover month to month. 

## GitLab Inc. United States Leave Policy:

Based on the Family and Medical Leave Act, or [FMLA](https://www.dol.gov/whd/fmla/), US Employees are "eligible to take job-protected leave for specified family and medical reasons with continuation of group health insurance coverage under the same terms and conditions as if the employee had not taken leave." For more information on what defines an eligible employee and medical reason please visit the [Electronic Code of Federal Regulation](http://www.ecfr.gov/cgi-bin/text-idx?c=ecfr&sid=d178a2522c85f1f401ed3f3740984fed&rgn=div5&view=text&node=29:3.1.1.3.54&idno=29#sp29.3.825.b) for the most up to date data.

### Apply For Parental Leave in the US

1. The employee will fill out the [Leave Request Form](https://drive.google.com/file/d/0B4eFM43gu7VPazNlQ3ktMmVVQVl6UmowZFRlQXdtaVExOW8w/view?usp=sharing) at least thirty days before the start of the leave (or as soon as reasonable) and email the form to People Ops.
  * Add your name, date of birth, address, social, and date of birth to the "To Be Completed By Employer" section. People Ops will fill out the rest of the form upon submission.
  * Employee will need to populate the "To Be Completed By The Claimant" and "To Be Completed By Attending Physician"
  * Review and sign the Disclosure Authorization. Also, review the Important Claim Notice.
1. People Ops will finish populating the "To Be Completed By the Employer/Administrator" and "Employers'Administrator's Certification" section of the form.
1. Once the form is complete, People Ops will email the form to Cigna using the email address in 1password.
1. Cigna will send a confirmation or denial of the claim via regular mail. Also, a Claims Manager will reach out to the employee by phone. 
1. TODO Outline letters to send and process to apply for STD.
  * If this is for a maternity leave, People Operations will explain the process for the employee to file for Short Term Disability (STD). [Short-Term Disability](/handbook/benefits/#short-and-long-term-disability-insurance) is processed through Cigna. If the employee is eligible for state STD, Cigna will advise on how to file.
1. Once the employee has filed a claim for STD, they will need to confirm the start and end dates of the STD.
  * The typical pay for STD is six weeks for a vaginal birth and eight weeks for a c-section. STD can be extended if the employee has a pregnancy related complication (bed rest, complications from delivery, etc).
1. People Operations will confirm [payroll details](#payroll-processing-during-parental-leave) with the Controller.   
1. The employee will notify People Operations on their first day back to work.
1. TODO Outline process to return the employee to work

### Payroll Processing During Parental Leave

**Paternity Leave**
Paternity Leave is not covered under Short Term Disability, so if the employee is eligible for 100% of pay, payroll would enter 100% of the pay period hours under "Leave with Pay."   

**Maternity Leave**
For maternity leave, GitLab will verify 100% of the wages are paid for eligible employees through payroll and Short-term Disability (STD).

1. While the employee is on short-term disability (which covers 66.7%), GitLab will supplement the remaining 33.3% through payroll.
  * The employee will inform People Operations when their short term disability is set to begin and end.
  * There is a seven day elimination period for short term disability that is not paid through Cigna, where GitLab will need to supplement the entire wage through payroll.
    * For instance, if Cigna has approved STD for January 1 - February 12, January 1 - January 7 would not be paid through STD.
    * If any adjustments need to be made to a payroll that has already passed, People Ops will coordinate with the Payroll Lead to ensure retroactive payments are made.
  * For the days the employee is paid through STD, payroll will adjust leave with pay hours to equal 33.3% and leave without pay hours to equal 66.7%.
    * For example, if there are 80 hours in the pay period you would input 26.66 hours Leave with Pay / 53.34 Leave without Pay.
1. When short-term disability ends, payroll will need to have 100% of the hours fall under leave with pay.

Note: Also, ensure the disability is not is not capped out at the current maximum or that will also need to be supplemented in each pay cycle.
