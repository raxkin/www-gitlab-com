---
layout: markdown_page
title: Product categories
extra_js:
    - listjs.min.js
    - categories.js
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Interfaces

We want intuitive interfaces both within the company and with the wider
community. This makes it more efficient for everyone to contribute or to get
a question answered. Therefore, the following interfaces are based on the
product categories defined on this page:

- [Home page](/)
- [Product page](/product/)
- [Product Features](/features/)
- [Pricing page](/pricing/)
- [DevOps Lifecycle](/stages-devops-lifecycle/)
- [DevOps Tools](/devops-tools/)
- [Product Vision](/direction/#vision)
- [Stage visions](/direction/#devops-stages)
- [Documentation](https://docs.gitlab.com/ee/#complete-devops-with-gitlab)
- [Engineering](https://about.gitlab.com/handbook/engineering/) Engineering Manager/Developer/Designer titles, their expertise, and department, and team names.
- [Product manager](https://about.gitlab.com/handbook/product/) responsibilities which are detailed on this page
- [Our pitch deck](https://about.gitlab.com/handbook/marketing/product-marketing/#company-pitch-deck), the slides that we use to describe the company
- [Product marketing](https://about.gitlab.com/handbook/marketing/product-marketing/) specializations

## Hierarchy

The categories form a hierarchy:

1. **Departments**: Dev, Ops, Sec, and Non DevOps (NDO). Maps to departments in our [organization structure](https://about.gitlab.com/company/team/structure/#table). At GitLab the Dev and Ops split is differently than the infinity loop suggests because our CI/CD functionality is one codebase, so from verify on we consider it Ops so that the codebase falls under one department.
1. **Stages**: Stages start with the 7 **loop stages**, then add Manage and Secure to get the 9 (DevOps) **value stages**, and then add the (Enablement) **team stage**. Values stages are what we all talk about in our marketing.
1. **Group**: Many stages have more than one [stage group](/company/team/structure/#stage-groups). Each of the stage groups has a dedicated backend engineering team. Within shared functions, like quality and product management, individuals are paired to one or more stages so that there are stable counterparts.
1. **Categories**: High-level capabilities that may be a standalone product at another company. e.g. Portfolio Management. There are a maximum of 8 high-level capabilities per stage to ensure we can display this on our website and pitch deck. There may need to be fewer categories, or shorter category names, if the aggregate number of lines when rendered would exceed 13 lines, when accounting for category names to word-wrap, which occurs at approximately 15 characters.
1. **Features**: Small, discrete functionalities. e.g. Issue weights. Some common features are listed within parentheses to facilitate finding responsible PMs by keyword. Features are maintained in [features.yml](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/features.yml).

Every category listed on this page must have a link, determined by what exists
in the following hierarchy:

Marketing product page > docs page > epic > label query > issue

E.g if there's no marketing page, link to the docs. If there's no docs, link to
the Epic. etc.

[Solutions](#solutions) can consist of multiple categories as defined on this
page, but there are also other ones, for example industry verticals. Solutions typically represent a customer challenge, how GitLab capabilities come together to meet that challenge, and business benefits of using our solution.

Capabilities can refer to stages, categories, features, but not solutions.

Adding more layers to the hierarchy would give it more fidelity but would hurt
usability in the following ways:

1. Harder to keep the [interfaces](#Interfaces) up to date.
1. Harder to automatically update things.
1. Harder to train and test people.
1. Harder to display more levels.
1. Harder to reason, falsify, and talk about it.
1. Harder to define what level something should be in.
1. Harder to keep this page up to date.

## Changes

The impact of changes to stages is felt [across the company](/company/team/structure/#stage-groups).
Merge requests with changes to stages and significant changes to categories need
to be approved by:

1. Head of Product
1. VP of Engineering
1. CEO needs to merge the change

## DevOps Stages

![DevOps Loop](devops-loop-and-spans.png)

<%= partial("includes/product/categories") %>

## Maturity

Not all categories are at the same level of maturity. Some are just minimal and
some are lovable. See the [category maturity page](/handbook/product/categories/maturity/) to see where each
category stands.

## Solutions

GitLab also does the things below that are composed of multiple categories.

1. Software Composition Analysis (SCA) = Dependency Scanning + License Management
1. Application Performance Monitoring (APM) = Metrics + Tracing + Real User Monitoring (RUM)

## Other functionality

This list of other functionality so you can easily find the team that owns it.
Maybe we should make our features easier to search to replace the section below.

### Other functionality in Manage

- User management, LDAP, signup, abuse, profiles
- Projects (Project creation, project templates, project import/export, importers)
- Groups and Subgroups
- GitLab.com specific functionality
- Navigation
- Admin Area

### Other functionality in Plan

- markdown functionality
- assignees
- milestones
- time tracking
- due dates
- labels
- issue weights
- quick actions
- email notifications
- todos
- search
- Elasticsearch integration
- Jira and other third-party issue management integration
- [gitlab-elasticsearch-indexer](https://gitlab.com/gitlab-org/gitlab-elasticsearch-indexer)

### Other functionality in Create

- [gitlab-shell](https://gitlab.com/gitlab-org/gitlab-shell)
- [gitlab-workhorse](https://gitlab.com/gitlab-org/gitlab-workhorse)

### Other functionality in Verify

- [Runner](https://docs.gitlab.com/runner/)

### Other functionality in Secure

- [Group Security Dashboard](https://docs.gitlab.com/ee/user/group/security_dashboard/)
- [Security reports](https://docs.gitlab.com/ee/user/project/merge_requests/#security-reports-ultimate)
