---
layout: markdown_page
title: "Analyst Relations"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Analyst relations at GitLab

Analyst relations (AR) is generally considered to be a corporate strategy, communications and marketing activity, but at GitLab, because of our mission that everyone can contribute, we view the analyst community as participants as well. The primary owner of Analyst Relations at GitLab is Product Marketing, as they are the conduit between the analysts and the internal company information.  

Industry analysts usually focus on corporate or organizational buyers and their needs rather than on those of individual developers. At GitLab we view them as a marketing focus for those markets. They amplify our message to a different group than our traditional developer communities, although there is overlap to some degree between those communities.

## How we interact with the analyst community

Examples of how we engage with analysts include:
- Schedule briefings where we update analysts on our product, our company, our future direction and strategy;
- Answer questions for analyst reports such as Gartner MQs or Forrester Waves that provide in-depth information on product features, customer references and company information;
- Use analyst reports such as Gartner MQs or Forrester Waves that feature GitLab to help enhance or clarify our story for customers and partners;
- Provide our analyst newsletter to update analysts on what GitLab is doing between briefings;
   - [GitLab Analyst Newsletter October 2018](https://docs.google.com/document/d/1MrAsx2UgIQjwbIEplrqvUvZajEzVRuKfZOGhU-qTExw/edit?usp=sharing)
- Schedule inquiries where analysts answer specific questions we have about products, markets, or strategies we want to understand better;
- Schedule consulting days for an extended dive with an analyst into products, markets, or strategies we are working on;
- Invite analysts to participate in webinars, speaking engagements, quotes for media, or other events where an analyst presence would be beneficial; and
- Hire analyst’s market research departments to help us create, run, and interpret survey research that helps us target markets or develop products optimally.

## Responding to vendor product comparison reports (e.g. MQs or Waves)

- GitLab will participate in any Wave or MQ to which we are invited to participate. We will commit to our best possible answers for the questionnaires.
- The parts of the team responsible for answering the questionnaire will make this a priority. We will:

   - Present every feature in the best possible light so we have the most defensible chance at high scores.
   - Provide whatever evidence we can that even hints at what the analysts are looking for.
   - When responding to the analyst request, challenge ourselves to find a way to honestly say “yes” and paint the product in the best light possible. Often, at first glance if we think we don’t support a feature or capability, with a bit of reflection and thought we can adapt our existing features to solve the problem at hand. This goes much smoother if we follow the first point and spend ongoing time with our analyst partners.
   - Even if we don't score well on the product today, we *must* score well on strategy.
   
- Once the report is published, we will create a microsite for each report that details the strengths and weaknesses and provides a link to relevant epics and issues that show how we are working to improve areas mentioned in the report. This becomes the basis for responding to the next iteration of that report, to continue the dialogue with the analysts, as well as to establish or demonstrate our thought leadership or go-to-market in that space.

## Accessing analyst reports

Most analyst companies charge for access to their reports.

-   If GitLab purchases reprint rights to a report, then that link will be available here, on the [Analyst Relations web page](https://about.gitlab.com/analysts/), and on the relevant product page. Reprint rights are the rights to share the link to the report - these generally last six months to one year.
-   GitLab maintains relationships with some analyst companies that provide us with access to some or all of their relevant research. These reports are for internal use only and sometimes limited to named individuals.  Those reports are generally kept in an internal [GitLab folder for analyst relations](https://drive.google.com/drive/u/0/folders/1oFmtmoXsbjMb6IuPIgIIZ-MG-tLfOjpw). If you are a GitLabber and you need access to a particular report, please reach out to [Analyst Relations](mailto:analysts@gitlab.com) and I'll help you find the research you need.

## What the analysts are saying about GitLab

This section contains highlights from areas where the analysts have rated GitLab in comparison to other vendors in a particular space. The highlights and lessons learned are listed here.  For more information click the link to go to the page dedicated to that report.

##### [The Forrester New Wave™: Value Stream Management Tools, Q3 2018](https://about.gitlab.com/analysts/forrester-vsm/)
##### [The Gartner Magic Quadrant for Application Release Orchestration, 2018](https://about.gitlab.com/analysts/gartner-aro/)

## Analyst reports that can help you deepen your knowledge
As part of XDR and Sales Enablement, some analyst reports are educational - they can help you build your understanding of the market. We've collected some of those articles to share with you here. These are behind the firewall and for use of employees where they have access rights. If you have any questions on access, please contact [Analyst Relations](mailto:analysts@gitlab.com).

[The complete list of documents by topic can be found here.](https://about.gitlab.com/handbook/marketing/product-marketing/analyst-relations/sales-training/)
