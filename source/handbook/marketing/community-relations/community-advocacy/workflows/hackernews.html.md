---
layout: markdown_page
title: "Hacker News"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

Most of our releases end up on Hacker News. Some of them hit the first page. These Hacker News posts are top priority for us to answer to. They're both [important and urgent](/handbook/marketing/community-relations/community-advocacy/guidelines/general.html#urgent-and-important-mentions).

_Every comment should get a response from someone from the company._ Feel free to use [this template](/handbook/marketing/community-relations/community-advocacy/#involving-experts). If you or the expert don't know the answer to a comment / remark please share your thoughts because every remark should have at least one response.

The [#hn-mentions](https://gitlab.slack.com/messages/hn-mentions) Slack channel tracks mentions of GitLab on HackerNews. It is a dedicated one, so that Community Advocates enable channel notifications and can respond to them as soon as possible.

## Workflow

![Hacker News channel workflow](/images/handbook/marketing/community-relations/hn-mentions.png){: .shadow}

1. React to notifications and go to the [#hn-mentions](https://gitlab.slack.com/messages/hn-mentions) channel when you see them appear
1. Read every new message in the channel and make a decision for each one: does it need a response?
1. Respond to the message if necessary, or [involve an expert](/handbook/marketing/community-relations/community-advocacy/#involving-experts) using the template
1. Ping `@sytse` in the `#community-relations` Slack channel if you judge his input is required, or in case of doubt
1. Add a checkmark (`:heavy_check_mark:`) to the message on the Slack channel

<i class="fas fa-hand-point-right" aria-hidden="true" style="color: rgb(138, 109, 59)
;"></i> Every message should have a checkmark to indicate that it was processed: either someone responded to the mention or decided that it didn't need a response.
{: .alert .alert-warning}

<i class="fas fa-info-circle" aria-hidden="true" style="color: rgb(49, 112, 143)
;"></i> Besides [#hn-mentions](https://gitlab.slack.com/messages/hn-mentions) channel, mentions of GitLab on HackerNews are also piped into the [#mentions-of-gitlab](/handbook/marketing/community-relations/community-advocacy/workflows/inactive.html/#mentions-of-gitlab-slack-channel) Slack channel.
{: .alert .alert-info}

## Best practices

When responding to a post about GitLab on Hacker News:

* Post a link to the thread in the `#community-relations` Slack channel and ping other `@advocates`.
* Don't post answers that are almost the same, link to the original one instead.
* Address multi-faceted comments by breaking them down and using points, numbering and quoting.
* When someone posts a HackerNews thread link, monitor that thread manually. Don't wait for the notifications in the [#hn-mentions](https://gitlab.slack.com/messages/hn-mentions) channel, because sometimes they're delayed by a few hours.

<i class="fas fa-info-circle" aria-hidden="true" style="color: rgb(49, 112, 143)
;"></i> Always bear in mind the [social media guidelines for Hacker News](/handbook/marketing/social-media-guidelines/#hacker-news) in all your interactions with the site.
{: .alert .alert-info}

## Automation

These mentions are piped to the [#hn-mentions](https://gitlab.slack.com/messages/hn-mentions) Slack channel by [Zapier](https://zapier.com).
