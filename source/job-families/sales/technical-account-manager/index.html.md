---
layout: job_family_page
title: "Technical Account Manager"
---

Are you passionate about customer success?  Do you have a proven customer acumen with a solid technical foundation? Then come be a member of GitLab's Customer Success Team. Providing guidance, planning and oversight while leveraging adoption and technical best practices. The Technical Account Manager is the key partner helping customers achieve their strategic objectives and maximum value from their investment in GitLab.  Additionally, the TAM serves as the liaison between the customer and the GitLab ecosystem, streamlining collaboration with Product Management, Engineering, Sales, Professional Services and others.

To learn more, see the [Technical Account Manager handbook](/handbook/customer-success/tam)

### Responsibilities

- Provide immediate on-boarding activities such as installation and training following investment of GitLab
- Own overall relationship with assigned clients, which include: increasing adoption, ensuring retention, and satisfaction
- Work with clients to build Customer Success Plans, establishing critical goals, or other key performance indicators and aid the customer in achieving their goals
- Measure and monitor customers achievement of critical and key performance indicators, reporting both internally to GitLab account stakeholders and externally to Customer Sponsors and Executives
- Establish regular cadence (weekly, Monthly, Quarterly) with each assigned clients, to review health metrics
- Establish a trusted/strategic advisor relationship with each assigned client and drive continued value of our solution and services
- Work closely with the GitLab Sales Account team (Account Executive, Solutions Architects, Professional Services) to identify opportunities for new usage of GitLab across organizational functions
- Work to identify and/or develop up-sell opportunities
- Advocate customer needs/issues cross-departmentally
- Program manage account escalations
- Assist and provide expert deployment, operational best practices and establishing a GitLab Center of Excellence
- Assist in workshops to help customers leverage the full value of GitLab solution
- Provide insights with respect to the availability and applicability of new features in GitLab
- Support GitLab Services in identifying and recommending training opportunities
- Act as the GitLab liaison for GitLab technical questions, issues or escalations.  This will include working with GitLab Support, Product Management(i.e. roadmaps), or others needed
- Maintain current functional knowledge and technical knowledge of GitLab platform

### Requirements

- 7 + years of experience in a related function is required with direct customer advocacy and engagement experience in post-sales or professional services functions
- Prior experience in Customer Success or equivalent history of increasing satisfaction, adoption, and retention
- Familiarity working with clients of all sizes, especially large enterprise organizations
- Exception verbal, written, organizational, presentation, and communications skills
- Detailed oriented and analytical
- Strong team player but self starter
- Strong technical, analytic and problem solving skills
- Experience with Ruby on Rails applications and Git
- Deep knowledge of software development lifecycle and development pipeline
- Understanding of continuous integration, continuous deployment, chatOps, and cloud native
- Above average knowledge of Unix and Unix based Operating Systems
- Installation and operation of Linux operating systems and hardware investigation/manipulation commands
- BASH/Shell scripting including systems and init.d startup scripts
- Package management (RPM, etc. to add/remove/list packages)
- Understanding of system log files/logging infrastructure
- B.Sc. in Computer Science or equivalent experience
- Programming/scripting experience & skill is required (Bash & Ruby)
- Project management experience & skills
- SCM admin and/or PS experience would be a plus
- Set up HA/DR, working with Containers and Schedulers (Kubernetes preferred) and also experience with AWS stack (EC2, ECS, RDS, ElastiCache)

## Specialties

Read more about what a [specialty](/handbook/hiring/#definitions) is at GitLab.

### Federal

#### Additional Additional Requirements

- TS/SCI Security Clearance
- Knowledge of and at least 4 years of experience with Federal customers

## Hiring Process

Candidates can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

- Qualified candidates for Technical Account Manager will receive a short questionnaire
- Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with a member of our Recruiting team
- Candidates will be invited to schedule an interview with our Director of Customer Success
- Next, candidates for the Technical Account Manager role will be invited to schedule an interview with members of the Customer Success team and may be asked to participate in a demo of a live install of GitLab
- Candidates for the Federal Technical Account Manager role will be invited to schedule interviews with members of the Customer Success team and our Federal Regional Sales Director
- Candidates may be invited to schedule an interview with our CRO
- Finally, candidates may interview with our CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing/).
