---
layout: job_family_page
title: "Engineering Manager, Quality"
---

GitLab is looking for a motivated and experienced leader to grow our test automation efforts across the entire GitLab ecosystem. This position will be focused on growing and developing a team that is instrumental to GitLab’s future success, so your efforts will have a noticeable impact to both the company and product. In addition to the requirements below, successful candidates will demonstrate a passion for high quality software, strong engineering principles and methodical problem solving skills.

## Responsibilities

- Define the roadmap for GitLab’s test automation efforts, track and publish progress against that roadamp, and manage the team’s priorities
- Work across the company to define and implement mechanisms to inject testing earlier into the software development process
- Drive adoption of best practices in code health, testing, testability and maintainability. You should know about clean code, the test pyramid and champion these concepts.
- Analyze complex software systems and collaborate with others to improve the overall design, testability and quality.
- Track quality and test metrics, and work with other teams to incorporate lessons learned from this data
- Recruit automation engineers and grow the team to stay in step with the product roadmap and company hiring plan

## Requirements

- Experience managing a small team
- Strong experience developing in Ruby
- Strong experience using Git
- Experience with test automation tools like Capybara, Selenium
- Experience in software development and/or test automation
- Experience working with Docker containers
- Experience with AWS or Kubernetes
- Experience with Continuous Integration systems (e.g., Jenkins, Travis, GitLab)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Qualified candidates receive a short questionnaire from our Recruiting team
- Selected candidates will be invited to schedule a 30 minute screening call with our Recruiting team
- Next, candidates will be invited to schedule a 45 minute first interview with the Director of Quality
- Candidates will then be invited to schedule a 1 hour technical interview with a Quality Engineer
- Candidates will be invited to schedule a third 45 minute interview with our VP of Engineering
- Finally, candidates will schedule a 50 minute interview with our CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing).
